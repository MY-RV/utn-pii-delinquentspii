/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package FBMY.types;

/**
 *
 * @author Yor
 * @param <I>
 * @param <N>
 */
public class IdName<I,N> {
    public final I id;
    public final N name;
    public IdName(I Id, N Name) {
        this.name = Name;
        this.id = Id;
    }
}
