/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package FBMY.types;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author MYRV
 */
public class DateTime extends Date {
    public String pattern = "yyyy/MM/dd HH:mm:ss zzz";
    
    public DateTime(String value) { super(value); }
    public DateTime() { super(); }
    
    public static DateTime valueOf(String value) {
        value = value.replace("-", "/");
        return new DateTime(value);
    }
    
    @Override
    public String toString() {
        var simpleDateFormat = new SimpleDateFormat(pattern);
        return simpleDateFormat.format(this);
    }
    
    public String toString(String pattern) {
        this.pattern = pattern;
        return toString();
    }
}
