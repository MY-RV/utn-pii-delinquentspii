/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package FBMY.treats.dataWorkers;

import FBMY.types.DateTime;
import java.util.*;

/**
 *
 * @author MYRV
 */
public class Parrot {
    public static interface parser { public Object parse(Object value); }
    private static String str(Object obj) { return String.valueOf(obj); }
    private static final HashMap<Class, parser> knowledge = new HashMap<>(){{
        put(String.class,   (value) -> String.valueOf(value));
        put(Integer.class,  (value) -> Integer.valueOf(str(value)));
        put(Float.class,    (value) -> Float.valueOf(str(value)));
        put(Boolean.class,  (value) -> Boolean.valueOf(str(value)));
        put(DateTime.class, (value) -> DateTime.valueOf(str(value)));
        put(UUID.class,     (value) -> UUID.fromString(str(value)));
    }};
    
    public static <T>T Translate(Class<T> key, Object value) {
        try {
            var parser = knowledge.get(key);
            if (parser == null && key.isEnum()) 
                return EnumTranslate(key, value.toString());
            return (T) parser.parse(value);
        } catch (Exception e) {
            return null;
        }
    }
    
    private  static <T>T EnumTranslate(Class<T> key, String value) {
        value = value.toLowerCase();
        var constants = key.getEnumConstants();
        for (var constant : constants) 
            if (value.equals(constant.toString().toLowerCase())) return constant;
        return null;
    }
    
    public static void learn(Class key, parser func) {
        if (knowledge.get(key) != null) return;
        knowledge.put(key, func);
    }
}
