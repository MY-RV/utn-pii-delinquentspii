/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package FBMY.treats.dataWorkers;

import FBMY.util.Closures.IRunnable;
import FBMY.util.Reflect;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Parameter;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author MYRV
 * @param <T>
 */
public class Racoon<T> {
    private final Cooper valMap = new Cooper();
    private final Class<T> recordClass;
    private final Constructor recordConstructor;
    private final Parameter recordParams[];
    
    public static <T>Racoon<T> For(Class<T> RecordClass) {
        return new Racoon(RecordClass);
    }
    private Racoon(Class<T> RecordClass) {
        this.recordClass = RecordClass;
        this.recordConstructor = recordClass.getConstructors()[0];
        this.recordParams = recordConstructor.getParameters();
    }
    
    private Object[] buildParams() { // Posible Desaparición
        var response = new Object[recordParams.length];
        for (int x = 0; x < response.length; x++) {
            var param = recordParams[x];
            var value = this.valMap.get(param.getName());
            response[x] = Parrot.Translate(param.getType(), value);
        } return response;
    }
     
    public T create(IRunnable<Cooper> builder) {
        builder.run(this.valMap);
        return build();
    }
    
    public T merge(T baseValues, T newValues) {
        var baseMap = toHashMap(baseValues);
        var newMap = toHashMap(newValues);
        
        for (var key : baseMap.keySet()) {
            var value = newMap.get(key);
            if (value == null) baseMap.get(key);
            valMap.set(key, value);
        }
        
        return build();
    }
    
    private T build() {
        var args = this.buildParams();
        this.valMap.clean();
        return newInstance(args);
    }
    
    private T newInstance(Object args[]) {
        try {
            return (T) recordConstructor.newInstance(args);
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Logger.getLogger(Racoon.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public static class Cooper { // Params Mapper For Raccon
        private HashMap<String, Object> valMap = new HashMap<>();
        protected Object get(String key) { return this.valMap.get(key); }
        protected void clean() { this.valMap = new HashMap<>(); }
        public Cooper set(String key, Object value) {
            valMap.put(key, value);
            return this;
        }

        @Override
        public String toString() {
            return valMap.toString();
        }
        
    }
    
    public HashMap<String, Object> toHashMap(T rcd) {
        HashMap<String, Object> response = new HashMap();
        for (var parameter : recordParams) {
            var param = parameter.getName();
            response.put(param, Reflect.runMethod(rcd, param));
        } return response;
    }
    
}
