/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package FBMY.treats.fileDealers.extension;

/**
 *
 * @author MYRV
 * @param <T>
 */
public interface IReader<T> {
    public interface PathPrefix { String get(); }
    public T getData();
    public Object withPrefix(IReader.PathPrefix prefixer);
    public String FilePath();
    
}
