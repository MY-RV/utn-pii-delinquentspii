/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package FBMY.treats.fileDealers;

import FBMY.util.Closures;
import FBMY.treats.dataWorkers.Racoon;
import FBMY.treats.fileDealers.dataDealers.DataCSV;
import FBMY.treats.fileDealers.extension.BFileTreat;
import FBMY.treats.fileDealers.extension.Parser;
import java.util.logging.*;
import java.util.*;
import java.io.*;

/**
 *
 * @author MYRV
 * @param <T>
 */
public class OwlCSV<T> extends BFileTreat<OwlCSV, DataCSV> {
    private final Racoon<T> racoon;
    
    public static <T>OwlCSV<T> For(
        Class<T> Maker, String FilePath
    ) { return new OwlCSV(Racoon.For(Maker), FilePath); }
    public static <T>OwlCSV<T> For(
        Racoon<T> Maker, String FilePath
    ) { return new OwlCSV(Maker, FilePath); }
    private OwlCSV(Racoon<T> Maker, String FilePath) {
        super(FilePath);
        this.racoon = Maker;
    }
    
    @Override
    public DataCSV<T> getData() { return getData(null); }
    public DataCSV<T> getData(Closures.IDataRowTrait<T> trait) {
        var response = DataCSV.For(racoon, trait);
        try (var file = new Scanner(new File(this.FilePath()))) {
            response.headers = Parser.csvLine.from(file.nextLine());
            while (file.hasNextLine()) response.addRow(file.nextLine());
            file.close();
            return response;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(OwlCSV.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public void setData(DataCSV<T> data) {
        try (var file = new BufferedWriter(new FileWriter(FilePath()))) {
            var headers = Arrays.toString(data.headers);
            file.write(headers.substring(1, headers.length()-1) + "\r");
            for (var row : data.rows) 
                file.write(newLine(data.headers, racoon.toHashMap(row)));
            file.close();
        } catch (IOException ex) {
            Logger.getLogger(OwlCSV.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void addData(T newRow) {
        var data = getData();
        data.rows.add(newRow);
        setData(data);
    }
    
    public void putData(Closures.ICondition<T> condition, T rcd) {
        setData(getData((row) -> {
            if (condition.run(row)) return rcd;
            return row;
        }));
    }
    
    public void delData(Closures.ICondition<T> condition) {
        setData(getData((row) -> {
            if (condition.run(row)) return null;
            return row;
        }));
    }
    
    private String newLine(String[] headers, HashMap<String, Object> map) {
        var response = "";
        for (var header : headers) {
            if (!response.equals("")) response += ",";
            var value = String.valueOf(map.get(header));
            if (value.split(",").length == 1) response += value;
            else response += "\"" + value + "\"";
        } return response+"\r";
    }
    
}
