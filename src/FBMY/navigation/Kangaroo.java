/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package FBMY.navigation;

import FBMY.util.Reflect;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author MYRV
 */
public class Kangaroo {
    static List<Frame> frames = new ArrayList<>();
    static Map<String, Frame> sideFrames = new HashMap<>();
    
    public static <T extends Controller>T goTo(
        Class<T> ctrlClass
    ) { return (T) goTo(ctrlClass, "index"); }
    public static <T extends Controller>T goTo(
        Class<T> ctrlClass, String methodName
    ) {
        clearFrames();
        var controller = Reflect.buildRawInstance(ctrlClass);
        frames.add(setUpFrame(controller));
        Reflect.runMethod(controller, methodName);
        return (T) controller;
    }

    
    public static <T extends Controller>T sideGet(
        String key, Class<T> ctrlClass
    ) { return sideGet(key, ctrlClass, "index"); }
    public static <T extends Controller>T sideGet(
        String key, Class<T> ctrlClass, String methodName
    ) {
        var frame = sideFrames.get(key);
        if (frame != null) frame.setVisible(false);
        var controller = Reflect.buildRawInstance(ctrlClass);
        frame = controller.frame;
        frame.controller = controller;
        sideFrames.put(key, frame);
        Reflect.runMethod(controller, methodName);
        return controller;
    }

    
    static void clearFrames() {
        var framesCount = frames.size();
        for (int i = 0; i < framesCount; i++) {
            frames.get(i).setVisible(false);
            frames.remove(i);
        }
        for (var entry : sideFrames.entrySet()) {
            entry.getValue().setVisible(false);
        }
        sideFrames = new HashMap<>();
    }
    
    static Frame setUpFrame(Controller controller) {
        var frame = controller.getFrame();
        return setUpFrame(frame, controller);
    }
    static Frame setUpFrame(Frame frame, Controller controller) {
        frame.controller = controller;
        return setUpFrame(frame);
    }
    static Frame setUpFrame(Frame frame) {
        frame.setLocationRelativeTo(null);
        frame.setSize(800, 500);
        frame.setUp();
        return frame;
    }
    
}
