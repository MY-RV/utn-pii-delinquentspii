/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package FBMY.util;

/**
 *
 * @author MYRV
 */
public class Closures {
    public interface IDataRowTrait<T> { T run(T arg); }
    public interface IComparation<T> { boolean run(T arg1, T arg2); }
    public interface ICondition<T> { boolean run(T arg); }
    public interface IRunnable<T> { void run(T arg); }
    
}
