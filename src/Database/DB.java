package Database;

import FBMY.treats.dataWorkers.Racoon;
import java.sql.*;
import java.util.*;

public class DB {
    
    public interface ResultMapper<T> {
        T map(ResultSet result) throws SQLException;
    }
    
    public interface Preparator {
        void process(PreparedStatement stmt) throws SQLException;
    }
    
    private static Connection newConnection() throws SQLException, ClassNotFoundException {
        Class.forName("org.postgresql.Driver");
        return DriverManager.getConnection(
            "jdbc:postgresql://localhost:5432/delinquents",
            "postgres", 
            "secret"
        );
    }
    
    public static void Ping() {
        try (var cnn = newConnection()) {
            cnn.close();
        } catch (ClassNotFoundException| SQLException e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
    }
    
    public static <T>T RawQuery(String query, ResultMapper<T> mapper) {
        try (var conn = newConnection(); var stmt = conn.createStatement()) {
            var response = mapper.map(stmt.executeQuery(query));
            stmt.close();
            conn.close();
            return response;
        } catch (ClassNotFoundException| SQLException e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            return null;
        }
    }
    
    public static boolean Execute(String query) {
        try (var conn = newConnection(); var stmt = conn.createStatement()) {
            stmt.execute(query);
            stmt.close();
            conn.close();
        } catch (ClassNotFoundException| SQLException e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            return false;
        }
        return true;
    }
    
    public static boolean Execute(String query, Preparator preparator) {
        try (var conn = newConnection(); var stmt = conn.prepareStatement(query)) {
            preparator.process(stmt);
            stmt.execute();
            stmt.close();
            conn.close();
        } catch (ClassNotFoundException| SQLException e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            return false;
        }
        return true;
    }
    
    public static <T extends Record>Collection<T> MultipleResults(Class<T> rClass, String query) {
        var response = new ArrayList<T>();
        try (var conn = newConnection(); var stmt = conn.createStatement()) {
            var result = stmt.executeQuery(query);
            var meta = result.getMetaData();
            
            var racoon = Racoon.For(rClass);
            while(result.next()) {
                var args = new HashMap<String, Object>();
                for (int i = 1; i <= meta.getColumnCount(); i++) 
                    args.put(meta.getColumnName(i), result.getObject(i));
                var item = racoon.create((r) -> args.forEach(r::set));
                response.add(item);
            }
            
            stmt.close();
            conn.close();
        } catch (ClassNotFoundException| SQLException e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
        return response;
    }
    
    public static <T extends Record>T SingleResult(Class<T> rClass, String query) {
        T response = null;
        try (var conn = newConnection(); var stmt = conn.createStatement()) {
            var result = stmt.executeQuery(query);
            var meta = result.getMetaData();
            
            var racoon = Racoon.For(rClass);
            result.next();
            var args = new HashMap<String, Object>();
            for (int i = 1; i <= meta.getColumnCount(); i++) 
                args.put(meta.getColumnName(i), result.getObject(i));
            response = racoon.create((r) -> args.forEach(r::set));
            
            stmt.close();
            conn.close();
        } catch (ClassNotFoundException| SQLException e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
        return response;
    }    
    
}
