package Database.Migrations;

import Database.DB;
import Database.Migrate;

public class DelinquentsChargesMigration implements Migrate.Migration {
    
    @Override
    public void migrate() {
        DB.Execute("""
            CREATE TABLE delinquents_charges (
                id SERIAL 		PRIMARY KEY,
            	delinquent_id           INTEGER REFERENCES delinquents(id),
            	charge_id 		INTEGER REFERENCES charges(id),
            	victims			INTEGER NULL,
            	created_at 		TIMESTAMP DEFAULT(NOW())
            );
        """);
    }
    
    @Override
    public void rollback() {
        DB.Execute("DROP TABLE IF EXISTS delinquents_charges");
    }
    
}
