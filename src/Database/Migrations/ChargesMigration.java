package Database.Migrations;

import Database.DB;
import Database.Migrate;

public class ChargesMigration implements Migrate.Migration {
    
    @Override
    public void migrate() {
        DB.Execute("""
            CREATE TABLE charges (
            	id SERIAL 		PRIMARY KEY,
            	name 			VARCHAR(50)
            );
        """);
    }
    
    @Override
    public void rollback() {
        DB.Execute("DROP TABLE IF EXISTS charges");
    }
    
}
