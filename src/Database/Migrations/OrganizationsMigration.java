package Database.Migrations;

import Database.DB;
import Database.Migrate;

public class OrganizationsMigration implements Migrate.Migration {
    
    @Override
    public void migrate() {
        DB.Execute("""
            CREATE TABLE organizations (
            	id SERIAL 		PRIMARY KEY,
            	name 			VARCHAR(50) UNIQUE
            );
        """);
    }
    
    @Override
    public void rollback() {
        DB.Execute("DROP TABLE IF EXISTS organizations");
    }
    
}
