package Database.Migrations;

import Database.DB;
import Database.Migrate;

public class CountriesMigration implements Migrate.Migration {
    
    @Override
    public void migrate() {
        DB.Execute("CREATE TABLE countries(name VARCHAR(50) UNIQUE)");
    }
    
    @Override
    public void rollback() {
        DB.Execute("DROP TABLE IF EXISTS countries");
    }
    
}
