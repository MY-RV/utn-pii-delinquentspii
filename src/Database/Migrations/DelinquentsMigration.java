package Database.Migrations;

import Database.DB;
import Database.Migrate;

public class DelinquentsMigration implements Migrate.Migration {
    
    @Override
    public void migrate() {
        DB.Execute("""
            CREATE TABLE delinquents (
            	id SERIAL 		PRIMARY KEY,
            	name 			VARCHAR(50),
            	surname 		VARCHAR(50),
            	alias			VARCHAR(50),
            	country			VARCHAR(50),
            	reward			INTEGER,
            	birth_date		TIMESTAMP,
            	agent_id 		INTEGER REFERENCES agents(id),
            	organization_id INTEGER REFERENCES organizations(id),
            	created_at 		TIMESTAMP DEFAULT(NOW())
            );
        """);
    }
    
    @Override
    public void rollback() {
        DB.Execute("DROP TABLE IF EXISTS delinquents");
    }
    
}
