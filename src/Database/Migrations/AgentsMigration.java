package Database.Migrations;

import Database.DB;
import Database.Migrate;

public class AgentsMigration implements Migrate.Migration {
    
    @Override
    public void migrate() {
        DB.Execute("""
            CREATE TABLE agents (
                id SERIAL 		PRIMARY KEY,
                name 			VARCHAR(50),
                surname 		VARCHAR(50),
                cellphone		VARCHAR(50) UNIQUE,
                created_at 		TIMESTAMP DEFAULT(NOW())
            );
        """);
    }
    
    @Override
    public void rollback() {
        DB.Execute("DROP TABLE IF EXISTS agents");
    }
    
}
