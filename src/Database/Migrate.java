package Database;

import Database.Migrations.*;
import Database.Seeders.*;
import FBMY.types.DateTime;

public class Migrate {

    public static interface Migration {
        void migrate();
        void rollback();
    }
    
    public static interface Seeder {
        void seed();
    }
    
    private static final int DOTS = 100;
    
    private static final Migration[] migrations = new Migration[] {
        new AgentsMigration(),
        new OrganizationsMigration(),
        new ChargesMigration(),
        new DelinquentsMigration(),
        new DelinquentsChargesMigration(),
        new CountriesMigration(),
    };
    
    private static final Seeder[] seeders = new Seeder[] {
        new AgentsSeeder(),
        new OrganizationsSeeder(),
        new ChargesSeeder(),
        new DelinquentsSeeder(),
        new DelinquentsChargesSeeder(),
        new CountriesSeeder(),
    };
    
    public static void main(String[] args) {
        Migrate.clearDb();
        System.out.println("");
        Migrate.fillDb();
        System.out.println("");
        Migrate.fillTables();
    }
    
    private static void clearDb() {
        System.out.println(new DateTime() + " Pruning Database");
        for (int i = migrations.length - 1; i >= 0; i--) {
            migrations[i].rollback();
        }
        System.out.println(new DateTime() + " Database Pruned");
    }
    
    private static void fillDb() {
        for (var migration : migrations) {
            var name = migration.getClass().getSimpleName();
            System.out.println(new DateTime() + " Migrating" + fillDots(name, DOTS));
            migration.migrate();
            System.out.println(new DateTime() + " Migrated." + fillDots(name, DOTS));
        }
    }
    
    private static void fillTables() {
        for (var seeder : seeders) {
            var name = seeder.getClass().getSimpleName();
            System.out.println(new DateTime() + " Seeding.." + fillDots(name, DOTS));
            seeder.seed();
            System.out.println(new DateTime() + " Seeded..." + fillDots(name, DOTS));
        }
    }
    
    private static String fillDots(String name, int lenght) {
        while (name.length() < lenght) {            
            name = "." + name;
        }
        return name;
    }
    
}
