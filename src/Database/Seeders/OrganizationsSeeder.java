package Database.Seeders;

import Database.DB;
import Database.Migrate;

public class OrganizationsSeeder implements Migrate.Seeder {
    
    @Override
    public void seed() {
        DB.Execute("""
            INSERT INTO organizations(name)
            SELECT * FROM ( VALUES
                ('Runolfsson, West and Hayes'),
                ('Simonis, Gottlieb and Zboncak'),
                ('Buckridge - Stoltenberg'),
                ('Schamberger - Wisozk'),
                ('Hahn - Ratke')
            )" "(name)
            WHERE name NOT IN(SELECT name FROM organizations);
        """);
    }
    
}
