package Database.Seeders;

import Database.DB;
import Database.Migrate;

public class AgentsSeeder implements Migrate.Seeder {
    
    @Override
    public void seed() {
        DB.Execute("""
            INSERT INTO agents(name, surname, cellphone)
            SELECT * FROM ( VALUES
                ('Marlon', 'Aufderhar', '1-905-657-5809 x916'),
                ('Cleo', 'Schinner', '995-510-4134 x7672'),
                ('Kaelyn', 'Gutkowski', '939-474-8551'),
                ('Christian', 'Hintz', '670.526.0792 x93541'),
                ('Rosalee', 'Mohr', '(827) 941-7696 x182'),
                ('Green', 'Klocko', '779.771.8199 x2980'),
                ('Willow', 'Wisoky', '352.721.2402 x611'),
                ('Benjamin', 'Skiles', '(463) 926-2659 x02083'),
                ('Karine', 'Gusikowski', '1-498-240-2834 x246'),
                ('Palma', 'Mayert', '666.623.8266 x7717'),
                ('Adrian', 'Herman', '1-509-844-6403 x05089'),
                ('Xander', 'Bogisich', '1-338-859-0030'),
                ('Lauryn', 'Roberts', '1-409-365-5247 x255'),
                ('Elliot', 'Rice', '(799) 742-7799 x9278'),
                ('Robert', 'Waters', '530-791-8833'),
                ('Mac', 'Quigley', '1-266-774-2793 x4910'),
                ('Therese', 'Ankunding', '661-485-7411 x6401'),
                ('Melody', 'Morar', '330.631.5545 x6732'),
                ('Reanna', 'Bosco', '(257) 425-7650 x659'),
                ('Dasia', 'Klocko', '588-428-5467 x6679'),
                ('Maggie', 'Senger', '448.640.5085'),
                ('Markus', 'Williamson', '(339) 749-1435 x69545'),
                ('Alek', 'Labadie', '(709) 227-1964 x886'),
                ('Kyra', 'Toy', '239-249-1607 x784'),
                ('Jose', 'Bartell', '875.362.5580 x838'),
                ('Jorge', 'Greenholt', '1-280-594-4152 x363'),
                ('Kelton', 'Yundt', '1-811-517-9839 x2620'),
                ('Griffin', 'Kshlerin', '1-895-252-8879'),
                ('Waylon', 'Jast', '1-626-630-4514 x20926'),
                ('Deron', 'Bradtke', '(363) 355-1919 x36789'),
                ('Daniela', 'Torphy', '1-640-916-6394 x381'),
                ('Vesta', 'Feest', '775.705.9509 x28727'),
                ('Roosevelt', 'Rutherford', '(370) 245-9486 x4184'),
                ('Darrion', 'Koch', '273.568.9835 x67275'),
                ('Jackeline', 'Hoeger', '588.874.1418 x9086'),
                ('Lora', 'Purdy', '434-739-7105 x33140'),
                ('Ian', 'Klocko', '688-263-4808 x9217'),
                ('Tad', 'Boyer', '1-207-683-2555'),
                ('Estefania', 'Koelpin', '442.978.8662'),
                ('Ed', 'Von', '629-216-6426')
            )" "(name, surname, cellphone)
            WHERE cellphone NOT IN (SELECT cellphone FROM agents);
        """);
    }
    
}
