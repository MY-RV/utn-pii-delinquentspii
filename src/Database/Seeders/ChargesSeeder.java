package Database.Seeders;

import Database.DB;
import Database.Migrate;

public class ChargesSeeder implements Migrate.Seeder {
    
    @Override
    public void seed() {
        DB.Execute("""
            INSERT INTO charges(name)
            SELECT * FROM (VALUES 
            	('Asesinato'),
                ('Asalto'),
                ('Extorción'),
                ('Estafas'),
                ('Alborotos'),
                ('Violencia'),
                ('Secuestro'),
                ('Violación'),
                ('Crímen Menor'),
                ('Vandalismo'),
                ('Compra y Venta de Drogas'),
                ('Carterista')
            )" "(name) WHERE name NOT IN(SELECT name FROM charges);
        """);
    }
    
}
