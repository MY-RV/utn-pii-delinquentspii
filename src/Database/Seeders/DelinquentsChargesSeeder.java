package Database.Seeders;

import Database.DB;
import Database.Migrate;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DelinquentsChargesSeeder implements Migrate.Seeder {
    
    @Override
    public void seed() {
        DB.RawQuery("""
            SELECT d.id, c.id
            FROM delinquents d, charges c
            ORDER BY RANDOM()
        """, (result) -> {
            try {
                var rand = new Random();
                var insertions = "";
                while (result.next() == true) {
                    if (rand.nextInt(100) < 90) continue;
                    if (!insertions.isEmpty()) insertions += ", ";
                    insertions += "(" + result.getInt(1) + ", " + result.getInt(2)+ ", " + rand.nextInt(1000) + ")";
                }
                DB.Execute(
                    "INSERT INTO delinquents_charges(delinquent_id, charge_id, victims) VALUES" + insertions
                );
            } catch (SQLException ex) {
                Logger.getLogger(DelinquentsChargesSeeder.class.getName()).log(Level.SEVERE, null, ex);
            }
            return true;
        });
    }
    
}
