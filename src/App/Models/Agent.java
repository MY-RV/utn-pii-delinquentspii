package App.Models;

import FBMY.types.DateTime;

public record Agent(
    Integer id, 
    String name, 
    String surname, 
    String cellphone, 
    DateTime created_at
) { }
