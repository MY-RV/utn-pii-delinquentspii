package App.Models;

import FBMY.types.DateTime;

public record Delinquent(
    Integer id, 
    String name, 
    String surname, 
    String alias, 
    String country, 
    Integer reward, 
    DateTime birth_date, 
    Integer agentId, 
    Integer organization_id, 
    DateTime created_at
) {}
