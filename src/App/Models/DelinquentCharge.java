package App.Models;

import FBMY.types.DateTime;

public record DelinquentCharge(
    Integer delinquentId, 
    Integer chargeId, 
    Integer victims, 
    DateTime created_at    
) { }
