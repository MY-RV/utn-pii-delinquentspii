package App.Models;

public record IdAndName(
    Integer id,
    String name
) { }
