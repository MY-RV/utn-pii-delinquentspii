package App.Lib;

import App.Models.IdAndName;
import java.awt.Color;

public class ChargeInput extends javax.swing.JPanel {
    
    private final IdAndName charge;
    
    public ChargeInput(IdAndName charge) {
        initComponents();
        this.charge = charge;
        ChbCharge.setText(charge.name());
        SpnVictims.setEnabled(false);
        LblVictims.setForeground(Color.LIGHT_GRAY);
        setSize(250, 70);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        ChbCharge = new javax.swing.JCheckBox();
        SpnVictims = new javax.swing.JSpinner();
        LblVictims = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));
        setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(204, 204, 204)));
        setPreferredSize(new java.awt.Dimension(270, 311));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        ChbCharge.setBackground(new java.awt.Color(255, 255, 255));
        ChbCharge.setFont(new java.awt.Font("Segoe UI", 0, 16)); // NOI18N
        ChbCharge.setText("Charge Tittle");
        ChbCharge.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ChbChargeActionPerformed(evt);
            }
        });
        add(ChbCharge, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 0, 230, -1));

        SpnVictims.setFont(new java.awt.Font("Segoe UI", 0, 16)); // NOI18N
        SpnVictims.setModel(new javax.swing.SpinnerNumberModel(0, 0, null, 1));
        SpnVictims.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(204, 204, 204)));
        add(SpnVictims, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 30, 150, -1));

        LblVictims.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        LblVictims.setText("Victimas:");
        add(LblVictims, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, -1, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void ChbChargeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ChbChargeActionPerformed
        SpnVictims.setEnabled(ChbCharge.isSelected());
        LblVictims.setForeground(ChbCharge.isSelected() 
            ? Color.BLACK
            : Color.LIGHT_GRAY
        );
        SpnVictims.setValue(0);
    }//GEN-LAST:event_ChbChargeActionPerformed

    public boolean hasCharge() {
        return ChbCharge.isSelected();
    }
    
    public int getChargeId() {
        return this.charge.id();
    }
    
    public int getVictims() {
        return (Integer) this.SpnVictims.getValue();
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox ChbCharge;
    private javax.swing.JLabel LblVictims;
    private javax.swing.JSpinner SpnVictims;
    // End of variables declaration//GEN-END:variables
}
