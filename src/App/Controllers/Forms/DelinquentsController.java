package App.Controllers.Forms;

import App.Frames.Forms.DelinquentsFrame;
import FBMY.navigation.Controller;
import App.Models.IdAndName;
import java.util.List;
import Database.DB;
import FBMY.types.Request;

public class DelinquentsController extends Controller<DelinquentsFrame> {
    public DelinquentsController(DelinquentsFrame frame) { super(frame); }

    @Override
    public void index() {
        frame.setVisible(true);
    }
    
    public List<IdAndName> getCharges() {
        return (List<IdAndName>) DB.MultipleResults(IdAndName.class, "SELECT * FROM charges ORDER BY name");
    }
    
    public List<IdAndName> getOrganizations() {
        return (List<IdAndName>) DB.MultipleResults(IdAndName.class, "SELECT * FROM organizations  ORDER BY name");
    }
    
    public List<IdAndName> getAgents() {
        return (List<IdAndName>) DB.MultipleResults(IdAndName.class, "SELECT id, name FROM agents  ORDER BY name");
    }
    
    public List<IdAndName> getCountries() {
        return (List<IdAndName>) DB.MultipleResults(IdAndName.class, "SELECT null\"id\", name FROM countries  ORDER BY name");
    }
    
    public boolean registerDelinquent(Request args) {
        var success = DB.Execute("""
            INSERT INTO delinquents(name, surname, alias, country, reward, birth_date, agent_id, organization_id) 
            VALUES (?, ?, ?, ?, ?, ?::DATE, ?, ?)
        """, (stmt) -> {
            stmt.setString( 1, args.get("name").toString());
            stmt.setString( 2, args.get("surname").toString());
            stmt.setString( 3, args.get("alias").toString());
            stmt.setString( 4, args.get("country").toString());
            stmt.setInt(    5, Integer.valueOf(args.get("reward").toString()));
            stmt.setString( 6, args.get("birth_date").toString());
            stmt.setInt(    7, Integer.valueOf(args.get("agent").toString()));
            stmt.setInt(    8, Integer.valueOf(args.get("organization").toString()));
        });
        
        if (!success) return false;
        
        var charges = (List<int[]>) args.get("charges");
        if(charges.isEmpty()) return false;
        
        var id = DB.RawQuery("SELECT id FROM delinquents ORDER BY id DESC LIMIT 1", 
            (result) -> {
                result.next();
                return result.getInt(1);
            }
        );
        var values = "";
        for (var charge : charges) {
            if (!values.isBlank()) values += ",";
            values += "(" + id + ", " + charge[0] + ", " + charge[1] + ")";
        }
        
        return DB.Execute(
            "INSERT INTO delinquents_charges(delinquent_id, charge_id, victims) VALUES"
            + values
        );
    }
    
}
