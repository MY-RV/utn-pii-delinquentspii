package App.Controllers.Forms;

import App.Frames.Forms.AgentFrame;
import FBMY.navigation.Controller;
import java.util.Collection;
import java.util.ArrayList;
import FBMY.types.Request;
import Database.DB;

public class AgentsController extends Controller<AgentFrame> {
    
    public AgentsController(AgentFrame frame) { super(frame); }

    @Override
    public void index() {
        frame.setVisible(true);
    }
    
    public Collection<String> getAgentsPhones() {
        return DB.RawQuery("SELECT cellphone FROM agents", (result) -> {
            var collector = new ArrayList<String>();
            while (result.next())
                collector.add(result.getString(1));
            return collector;
        });
    }
    
    public boolean registerAgent(Request args) {
        return DB.Execute("INSERT INTO agents(name, surname, cellphone) VALUES (?, ?, ?)", 
            (stmt) -> {
                stmt.setString(1, args.get("name").toString());
                stmt.setString(2, args.get("surname").toString());
                stmt.setString(3, args.get("cellphone").toString());
            }
        );
    }
    
}
