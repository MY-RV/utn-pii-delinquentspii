package App.Controllers.Forms;

import App.Frames.Forms.OrganizationsFrame;
import FBMY.navigation.Controller;
import java.util.Collection;
import java.util.ArrayList;
import Database.DB;

public class OrganizationsController extends Controller<OrganizationsFrame> {
    
    public OrganizationsController(OrganizationsFrame frame) { super(frame); }

    @Override
    public void index() {
        frame.setVisible(true);
    }
    
    public Collection<String> getOrganizationsNames() {
        return DB.RawQuery("SELECT name FROM organizations ORDER BY name", (result) -> {
            var collector = new ArrayList<String>();
            while (result.next())
                collector.add(result.getString(1));
            return collector;
        });
    }
    
    public boolean registerOrganization(String name) {
        return DB.Execute("INSERT INTO organizations(name) VALUES (?)", 
            (stmt) -> stmt.setString(1, name)
        );
    }

}
