package App.Controllers.Dtos;

public record DelinquentDto(
    Integer id, 
    String alias, 
    String agent, 
    String charges, 
    String full_name, 
    String organization
) {
    public final static String query = """
    """;
}
