package App.Controllers;

import App.Controllers.Dtos.DelinquentDto;
import App.Frames.ReportsFrame;
import App.Models.IdAndName;
import Database.DB;
import FBMY.navigation.Controller;
import java.util.List;

public class ReportsController extends Controller<ReportsFrame> {
    public ReportsController(ReportsFrame frame) { super(frame); }

    @Override
    public void index() {
        frame.setVisible(true);
    }

    public List<IdAndName> getAgents() {
        return (List<IdAndName>) DB.MultipleResults(IdAndName.class, """
           SELECT id, name FROM agents ORDER BY name
        """);
    }
    
    public List<DelinquentDto> getDelinquentsByAgent(int id) {
        return (List<DelinquentDto>) DB.MultipleResults(DelinquentDto.class, """
            SELECT  d.id, 
                    d.alias,
                    CONCAT(d.name, ' ', d.surname)"full_name",
                    COALESCE(dc.charges, '')"charges",
                    o.name "organization",
                    a.name "agent"
            FROM delinquents d 
            JOIN agents a ON a.id = d.agent_id 
            JOIN organizations o ON o.id = d.organization_id
            LEFT JOIN (
                SELECT dc.delinquent_id, STRING_AGG(DISTINCT c.name, ', ')"charges" FROM charges c 
                JOIN delinquents_charges dc ON dc.charge_id = c.id
                GROUP BY dc.delinquent_id 
            ) dc ON dc.delinquent_id = d.id
            WHERE a.id = 
        """ + id);
    }
    
    public List<IdAndName> getChargesCounts() {
        return (List<IdAndName>) DB.MultipleResults(IdAndName.class, """
            SELECT COUNT(*)"id", c.name FROM charges c 
            JOIN delinquents_charges dc ON dc.charge_id = c.id
            GROUP BY c.id, c."name" ORDER BY COUNT(*) DESC
        """);
    }
    
    
    
    
}
