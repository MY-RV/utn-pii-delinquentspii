package App.Frames.Forms;

import App.Controllers.Forms.AgentsController;
import App.Controllers.Forms.DelinquentsController;
import App.Controllers.Forms.OrganizationsController;
import App.Controllers.ReportsController;
import FBMY.navigation.Kangaroo;
import javax.swing.JOptionPane;
import FBMY.navigation.Frame;
import App.Frames.MainFrame;
import App.Models.IdAndName;
import java.util.Collection;
import App.Lib.ChargeInput;
import FBMY.types.DateTime;
import java.util.ArrayList;
import FBMY.types.Request;
import java.awt.Dimension;
import java.awt.Color;
import java.util.List;

public class DelinquentsFrame extends Frame<DelinquentsController> {

    Collection<ChargeInput> charges = new ArrayList<>();
    List<IdAndName> organizations;
    List<IdAndName> agents;
    
    private final Color BtnCreateColor;
    
    public DelinquentsFrame() {
        initComponents();
        this.BtnCreateColor = BtnCreate.getBackground();
        BtnCreate.setBackground(Color.LIGHT_GRAY);
        BtnCreate.setEnabled(false);
    }

    @Override
    public void setUp() {
        this.setSize(610, 450);
        for (var charge : controller.getCharges()) {
            var component = new ChargeInput(charge);
            this.ScrCharges.add(component);
        }
        var items = (List<IdAndName>) controller.getCharges();
        for (int i = 0; i < items.size(); i++) {
            var component = new ChargeInput(items.get(i));
            this.PnlScroll.add(component);
            this.charges.add(component);
            component.setBounds(0, i * 70, 250, 70);
        }
        PnlScroll.setSize(250, 770);
        ScrCharges.setPreferredSize(new Dimension(270, 770));
        
        this.organizations = controller.getOrganizations();
        this.organizations.forEach((org) -> CbxOrganization.addItem(org.name()));
        
        this.agents = controller.getAgents();
        this.agents.forEach((org) -> CbxAgent.addItem(org.name()));
        
        controller.getCountries().forEach((org) -> CbxCountry.addItem(org.name()));
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        BgPanel = new javax.swing.JPanel();
        ScrCharges = new javax.swing.JScrollPane();
        PnlScroll = new javax.swing.JPanel();
        TxtSurname = new javax.swing.JTextField();
        LblSurname = new javax.swing.JLabel();
        TxtName = new javax.swing.JTextField();
        LblName = new javax.swing.JLabel();
        LblTitle = new javax.swing.JLabel();
        LblAlias = new javax.swing.JLabel();
        TxtAlias = new javax.swing.JTextField();
        BtnCreate = new javax.swing.JButton();
        LblCountry = new javax.swing.JLabel();
        LblReward = new javax.swing.JLabel();
        CbxCountry = new javax.swing.JComboBox<>();
        SpnReward = new javax.swing.JSpinner();
        CbxAgent = new javax.swing.JComboBox<>();
        LblAgent = new javax.swing.JLabel();
        CbxOrganization = new javax.swing.JComboBox<>();
        LblCountry2 = new javax.swing.JLabel();
        TxtBirthdate = new javax.swing.JTextField();
        LblBirthdate = new javax.swing.JLabel();
        AppMenu = new javax.swing.JMenuBar();
        Organization = new javax.swing.JMenu();
        Agent = new javax.swing.JMenuItem();
        Delinquent = new javax.swing.JMenuItem();
        DelinquentCharge = new javax.swing.JMenuItem();
        Reports = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        BgPanel.setBackground(new java.awt.Color(255, 255, 255));
        BgPanel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        ScrCharges.setBorder(null);
        ScrCharges.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        ScrCharges.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        ScrCharges.setViewportBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));

        PnlScroll.setBackground(new java.awt.Color(255, 255, 255));
        PnlScroll.setPreferredSize(new java.awt.Dimension(260, 1050));

        javax.swing.GroupLayout PnlScrollLayout = new javax.swing.GroupLayout(PnlScroll);
        PnlScroll.setLayout(PnlScrollLayout);
        PnlScrollLayout.setHorizontalGroup(
            PnlScrollLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 260, Short.MAX_VALUE)
        );
        PnlScrollLayout.setVerticalGroup(
            PnlScrollLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1050, Short.MAX_VALUE)
        );

        ScrCharges.setViewportView(PnlScroll);

        BgPanel.add(ScrCharges, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 20, 260, 230));

        TxtSurname.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        TxtSurname.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(204, 204, 204)));
        TxtSurname.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                TxtSurnameKeyReleased(evt);
            }
        });
        BgPanel.add(TxtSurname, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 120, 200, 30));

        LblSurname.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        LblSurname.setText("Apellido:");
        BgPanel.add(LblSurname, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 120, 80, 30));

        TxtName.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        TxtName.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(204, 204, 204)));
        TxtName.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                TxtNameKeyReleased(evt);
            }
        });
        BgPanel.add(TxtName, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 70, 200, 30));

        LblName.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        LblName.setText("Nombre:");
        BgPanel.add(LblName, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 70, 80, 30));

        LblTitle.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        LblTitle.setText("Nuevo Delincuente");
        BgPanel.add(LblTitle, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, 230, -1));

        LblAlias.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        LblAlias.setText("Apodo:");
        BgPanel.add(LblAlias, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 170, 90, 30));

        TxtAlias.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        TxtAlias.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(204, 204, 204)));
        TxtAlias.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                TxtAliasKeyReleased(evt);
            }
        });
        BgPanel.add(TxtAlias, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 170, 200, 30));

        BtnCreate.setBackground(new java.awt.Color(102, 204, 255));
        BtnCreate.setFont(new java.awt.Font("Segoe UI", 1, 20)); // NOI18N
        BtnCreate.setForeground(new java.awt.Color(255, 255, 255));
        BtnCreate.setText("Registrar");
        BtnCreate.setBorder(null);
        BtnCreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCreateActionPerformed(evt);
            }
        });
        BgPanel.add(BtnCreate, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 370, 110, 40));

        LblCountry.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        LblCountry.setText("País:");
        BgPanel.add(LblCountry, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 220, 90, 30));

        LblReward.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        LblReward.setText("Reward:");
        BgPanel.add(LblReward, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 270, 90, 30));

        CbxCountry.setBackground(new java.awt.Color(255, 255, 254));
        CbxCountry.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        CbxCountry.setBorder(null);
        BgPanel.add(CbxCountry, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 220, 200, -1));

        SpnReward.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        SpnReward.setModel(new javax.swing.SpinnerNumberModel(0, 0, null, 1));
        SpnReward.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(204, 204, 204)));
        BgPanel.add(SpnReward, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 270, 200, -1));

        CbxAgent.setBackground(new java.awt.Color(255, 255, 254));
        CbxAgent.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        CbxAgent.setBorder(null);
        BgPanel.add(CbxAgent, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 320, 180, -1));

        LblAgent.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        LblAgent.setText("Agente:");
        BgPanel.add(LblAgent, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 320, 90, 30));

        CbxOrganization.setBackground(new java.awt.Color(255, 255, 254));
        CbxOrganization.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        CbxOrganization.setBorder(null);
        BgPanel.add(CbxOrganization, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 320, 200, -1));

        LblCountry2.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        LblCountry2.setText("Banda:");
        BgPanel.add(LblCountry2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 320, 90, 30));

        TxtBirthdate.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        TxtBirthdate.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(204, 204, 204)));
        TxtBirthdate.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                TxtBirthdateKeyReleased(evt);
            }
        });
        BgPanel.add(TxtBirthdate, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 270, 150, 30));

        LblBirthdate.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        LblBirthdate.setText("Nacimiento:");
        BgPanel.add(LblBirthdate, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 270, 110, 30));

        AppMenu.setBackground(new java.awt.Color(240, 240, 240));
        AppMenu.setBorder(null);
        AppMenu.setBorderPainted(false);

        Organization.setText("Registrar");

        Agent.setText("Agente");
        Agent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AgentActionPerformed(evt);
            }
        });
        Organization.add(Agent);

        Delinquent.setText("Delincuente");
        Delinquent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DelinquentActionPerformed(evt);
            }
        });
        Organization.add(Delinquent);

        DelinquentCharge.setText("Organización");
        DelinquentCharge.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DelinquentChargeActionPerformed(evt);
            }
        });
        Organization.add(DelinquentCharge);

        AppMenu.add(Organization);

        Reports.setText("Reportes");
        Reports.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ReportsMouseClicked(evt);
            }
        });
        Reports.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ReportsActionPerformed(evt);
            }
        });
        AppMenu.add(Reports);

        setJMenuBar(AppMenu);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(BgPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 610, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(BgPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 430, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void TxtSurnameKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtSurnameKeyReleased
        this.validateForm();
    }//GEN-LAST:event_TxtSurnameKeyReleased

    private void TxtNameKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtNameKeyReleased
        this.validateForm();
    }//GEN-LAST:event_TxtNameKeyReleased

    private void TxtAliasKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtAliasKeyReleased
        this.validateForm();
    }//GEN-LAST:event_TxtAliasKeyReleased

    private void BtnCreateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCreateActionPerformed
        var success = controller.registerDelinquent(this.genPayload());
        if (success) {
            JOptionPane.showMessageDialog(this, "Delinquente Creado con Exito");
            Kangaroo.goTo(MainFrame.MainController.class);
        } else  {
            JOptionPane.showMessageDialog(this, "ALGO HA SALIDO MAL!");
        }
    }//GEN-LAST:event_BtnCreateActionPerformed

    private void TxtBirthdateKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtBirthdateKeyReleased
        this.validateForm();
    }//GEN-LAST:event_TxtBirthdateKeyReleased

    private void AgentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AgentActionPerformed
        Kangaroo.goTo(AgentsController.class);
    }//GEN-LAST:event_AgentActionPerformed

    private void DelinquentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DelinquentActionPerformed
        Kangaroo.goTo(DelinquentsController.class);
    }//GEN-LAST:event_DelinquentActionPerformed

    private void DelinquentChargeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DelinquentChargeActionPerformed
        Kangaroo.goTo(OrganizationsController.class);
    }//GEN-LAST:event_DelinquentChargeActionPerformed

    private void ReportsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ReportsMouseClicked
        Kangaroo.goTo(ReportsController.class);
    }//GEN-LAST:event_ReportsMouseClicked

    private void ReportsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ReportsActionPerformed
        Kangaroo.goTo(ReportsController.class);
    }//GEN-LAST:event_ReportsActionPerformed

    public void validateForm() {
        boolean isValid = true;
        if (TxtName.getText().isEmpty()) 
            isValid = false;
        if (TxtSurname.getText().isEmpty()) 
            isValid = false;
        if (TxtBirthdate.getText().isEmpty()) 
            isValid = false;
        try {
            new DateTime(TxtBirthdate.getText());
            LblBirthdate.setForeground(Color.BLACK);
        } catch (Exception e) {
            isValid = false;
            LblBirthdate.setForeground(Color.RED);
        }
        BtnCreate.setEnabled(isValid);
        BtnCreate.setBackground(isValid ? BtnCreateColor : Color.LIGHT_GRAY);
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem Agent;
    private javax.swing.JMenuBar AppMenu;
    private javax.swing.JPanel BgPanel;
    private javax.swing.JButton BtnCreate;
    private javax.swing.JComboBox<String> CbxAgent;
    private javax.swing.JComboBox<String> CbxCountry;
    private javax.swing.JComboBox<String> CbxOrganization;
    private javax.swing.JMenuItem Delinquent;
    private javax.swing.JMenuItem DelinquentCharge;
    private javax.swing.JLabel LblAgent;
    private javax.swing.JLabel LblAlias;
    private javax.swing.JLabel LblBirthdate;
    private javax.swing.JLabel LblCountry;
    private javax.swing.JLabel LblCountry2;
    private javax.swing.JLabel LblName;
    private javax.swing.JLabel LblReward;
    private javax.swing.JLabel LblSurname;
    private javax.swing.JLabel LblTitle;
    private javax.swing.JMenu Organization;
    private javax.swing.JPanel PnlScroll;
    private javax.swing.JMenu Reports;
    private javax.swing.JScrollPane ScrCharges;
    private javax.swing.JSpinner SpnReward;
    private javax.swing.JTextField TxtAlias;
    private javax.swing.JTextField TxtBirthdate;
    private javax.swing.JTextField TxtName;
    private javax.swing.JTextField TxtSurname;
    // End of variables declaration//GEN-END:variables

    
    private Request genPayload() {
        var payload = new Request();
        payload.put("name", TxtName.getText().trim());
        payload.put("surname", TxtSurname.getText().trim());
        payload.put("alias", TxtAlias.getText().trim());
        payload.put("country", CbxCountry.getSelectedItem());
        payload.put("birth_date", TxtBirthdate.getText());
        payload.put("reward", SpnReward.getValue());
        payload.put("organization", organizations.get(
            CbxOrganization.getSelectedIndex()
        ).id());
        payload.put("agent", agents.get(
            CbxAgent.getSelectedIndex()
        ).id());
        
        var Charges = new ArrayList<int[]>();
        this.charges.forEach((c) -> {
            if (!c.hasCharge()) return;
            Charges.add(new int[]{
               c.getChargeId(),
               c.getVictims(),
            });
        });
        
        payload.put("charges", Charges);
        return payload;
    }
}
