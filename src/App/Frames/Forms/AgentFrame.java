package App.Frames.Forms;

import App.Controllers.Forms.AgentsController;
import App.Controllers.Forms.DelinquentsController;
import App.Controllers.Forms.OrganizationsController;
import App.Controllers.ReportsController;
import FBMY.navigation.Kangaroo;
import javax.swing.JOptionPane;
import FBMY.navigation.Frame;
import App.Frames.MainFrame;
import java.util.Collection;
import FBMY.types.Request;
import java.awt.Color;

public class AgentFrame extends Frame<AgentsController> {

    Collection<String> agentsPhones;
    Color BtnCreateColor;
    
    public AgentFrame() {
        initComponents();
    }

    @Override
    public void setUp() {
        this.setSize(350, 300);
        this.agentsPhones = controller.getAgentsPhones();
        this.BtnCreateColor = BtnCreate.getBackground();
        BtnCreate.setBackground(Color.LIGHT_GRAY);
        BtnCreate.setEnabled(false);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        BgPanel = new javax.swing.JPanel();
        LblCellphone = new javax.swing.JLabel();
        TxtCellphone = new javax.swing.JTextField();
        LblSurname = new javax.swing.JLabel();
        TxtSurname = new javax.swing.JTextField();
        TxtName = new javax.swing.JTextField();
        LblName = new javax.swing.JLabel();
        LblTitle = new javax.swing.JLabel();
        BtnCreate = new javax.swing.JButton();
        AppMenu = new javax.swing.JMenuBar();
        Organization = new javax.swing.JMenu();
        Agent = new javax.swing.JMenuItem();
        Delinquent = new javax.swing.JMenuItem();
        DelinquentCharge = new javax.swing.JMenuItem();
        Reports = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        BgPanel.setBackground(new java.awt.Color(255, 255, 255));
        BgPanel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        LblCellphone.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        LblCellphone.setText("Telefono:");
        BgPanel.add(LblCellphone, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 170, 90, 30));

        TxtCellphone.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        TxtCellphone.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(204, 204, 204)));
        TxtCellphone.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                TxtCellphoneKeyReleased(evt);
            }
        });
        BgPanel.add(TxtCellphone, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 170, 220, 30));

        LblSurname.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        LblSurname.setText("Apellido:");
        BgPanel.add(LblSurname, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 120, 80, 30));

        TxtSurname.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        TxtSurname.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(204, 204, 204)));
        TxtSurname.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                TxtSurnameKeyReleased(evt);
            }
        });
        BgPanel.add(TxtSurname, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 120, 220, 30));

        TxtName.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        TxtName.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(204, 204, 204)));
        TxtName.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                TxtNameKeyReleased(evt);
            }
        });
        BgPanel.add(TxtName, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 70, 220, 30));

        LblName.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        LblName.setText("Nombre:");
        BgPanel.add(LblName, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 70, 80, 30));

        LblTitle.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        LblTitle.setText("Nuevo Agente");
        BgPanel.add(LblTitle, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, -1, -1));

        BtnCreate.setBackground(new java.awt.Color(102, 204, 255));
        BtnCreate.setFont(new java.awt.Font("Segoe UI", 1, 20)); // NOI18N
        BtnCreate.setForeground(new java.awt.Color(255, 255, 255));
        BtnCreate.setText("Registrar");
        BtnCreate.setBorder(null);
        BtnCreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCreateActionPerformed(evt);
            }
        });
        BgPanel.add(BtnCreate, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 220, 110, 40));

        AppMenu.setBackground(new java.awt.Color(240, 240, 240));
        AppMenu.setBorder(null);
        AppMenu.setBorderPainted(false);

        Organization.setText("Registrar");

        Agent.setText("Agente");
        Agent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AgentActionPerformed(evt);
            }
        });
        Organization.add(Agent);

        Delinquent.setText("Delincuente");
        Delinquent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DelinquentActionPerformed(evt);
            }
        });
        Organization.add(Delinquent);

        DelinquentCharge.setText("Organización");
        DelinquentCharge.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DelinquentChargeActionPerformed(evt);
            }
        });
        Organization.add(DelinquentCharge);

        AppMenu.add(Organization);

        Reports.setText("Reportes");
        Reports.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ReportsMouseClicked(evt);
            }
        });
        Reports.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ReportsActionPerformed(evt);
            }
        });
        AppMenu.add(Reports);

        setJMenuBar(AppMenu);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(BgPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 350, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(BgPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 280, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void TxtCellphoneKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtCellphoneKeyReleased
        this.validateForm();
    }//GEN-LAST:event_TxtCellphoneKeyReleased

    private void TxtSurnameKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtSurnameKeyReleased
        this.validateForm();
    }//GEN-LAST:event_TxtSurnameKeyReleased

    private void TxtNameKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtNameKeyReleased
        this.validateForm();
    }//GEN-LAST:event_TxtNameKeyReleased

    private void BtnCreateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCreateActionPerformed
        var success = controller.registerAgent(this.genPayload());
        if (success) {
            JOptionPane.showMessageDialog(this, "Agente Creado con Exito");
            Kangaroo.goTo(MainFrame.MainController.class);
        } else  {
            JOptionPane.showMessageDialog(this, "ALGO HA SALIDO MAL!");
        }
    }//GEN-LAST:event_BtnCreateActionPerformed

    private void AgentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AgentActionPerformed
        Kangaroo.goTo(AgentsController.class);
    }//GEN-LAST:event_AgentActionPerformed

    private void DelinquentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DelinquentActionPerformed
        Kangaroo.goTo(DelinquentsController.class);
    }//GEN-LAST:event_DelinquentActionPerformed

    private void DelinquentChargeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DelinquentChargeActionPerformed
        Kangaroo.goTo(OrganizationsController.class);
    }//GEN-LAST:event_DelinquentChargeActionPerformed

    private void ReportsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ReportsMouseClicked
        Kangaroo.goTo(ReportsController.class);
    }//GEN-LAST:event_ReportsMouseClicked

    private void ReportsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ReportsActionPerformed
        Kangaroo.goTo(ReportsController.class);
    }//GEN-LAST:event_ReportsActionPerformed

    public void validateForm() {
        var phoneInUse = this.agentsPhones.contains(TxtCellphone.getText().trim());
        if (phoneInUse
            || TxtName.getText().isBlank()
            || TxtSurname.getText().isBlank()
            || TxtCellphone.getText().isBlank()
        ) {
            BtnCreate.setEnabled(false);
            BtnCreate.setBackground(Color.LIGHT_GRAY);
        } else {
            BtnCreate.setEnabled(true);
            BtnCreate.setBackground(this.BtnCreateColor);
        }
        LblCellphone.setForeground(!phoneInUse 
            ? Color.BLACK
            : Color.RED 
        );
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem Agent;
    private javax.swing.JMenuBar AppMenu;
    private javax.swing.JPanel BgPanel;
    private javax.swing.JButton BtnCreate;
    private javax.swing.JMenuItem Delinquent;
    private javax.swing.JMenuItem DelinquentCharge;
    private javax.swing.JLabel LblCellphone;
    private javax.swing.JLabel LblName;
    private javax.swing.JLabel LblSurname;
    private javax.swing.JLabel LblTitle;
    private javax.swing.JMenu Organization;
    private javax.swing.JMenu Reports;
    private javax.swing.JTextField TxtCellphone;
    private javax.swing.JTextField TxtName;
    private javax.swing.JTextField TxtSurname;
    // End of variables declaration//GEN-END:variables

    private Request genPayload() {
        var payload = new Request();
        payload.put("name", TxtName.getText().trim());
        payload.put("surname", TxtSurname.getText().trim());
        payload.put("cellphone", TxtCellphone.getText().trim());
        return payload;
    }
}
