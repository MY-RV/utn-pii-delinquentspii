package App.Frames.Forms;

import App.Controllers.Forms.AgentsController;
import App.Controllers.Forms.DelinquentsController;
import App.Controllers.Forms.OrganizationsController;
import App.Controllers.ReportsController;
import FBMY.navigation.Kangaroo;
import javax.swing.JOptionPane;
import FBMY.navigation.Frame;
import java.util.Collection;
import App.Frames.MainFrame;
import java.awt.Color;

public class OrganizationsFrame extends Frame<OrganizationsController> {

    Collection<String> organizations;
    Color BtnCreateColor;
    
    public OrganizationsFrame() {
        initComponents();
    }

    @Override
    public void setUp() {
        this.setSize(350, 200);
        this.organizations = controller.getOrganizationsNames();
        this.BtnCreateColor = BtnCreate.getBackground();
        BtnCreate.setBackground(Color.LIGHT_GRAY);
        this.BtnCreate.setEnabled(false);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        BgPanel = new javax.swing.JPanel();
        LblName = new javax.swing.JLabel();
        TxtName = new javax.swing.JTextField();
        LblTitle = new javax.swing.JLabel();
        BtnCreate = new javax.swing.JButton();
        AppMenu = new javax.swing.JMenuBar();
        Organization = new javax.swing.JMenu();
        Agent = new javax.swing.JMenuItem();
        Delinquent = new javax.swing.JMenuItem();
        DelinquentCharge = new javax.swing.JMenuItem();
        Reports = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        BgPanel.setBackground(new java.awt.Color(255, 255, 255));
        BgPanel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        LblName.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        LblName.setText("Nombre:");
        BgPanel.add(LblName, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 70, 80, 30));

        TxtName.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        TxtName.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(204, 204, 204)));
        TxtName.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                TxtNameKeyReleased(evt);
            }
        });
        BgPanel.add(TxtName, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 70, 220, 30));

        LblTitle.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        LblTitle.setText("Nueva Organización");
        BgPanel.add(LblTitle, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, -1, -1));

        BtnCreate.setBackground(new java.awt.Color(102, 204, 255));
        BtnCreate.setFont(new java.awt.Font("Segoe UI", 1, 20)); // NOI18N
        BtnCreate.setForeground(new java.awt.Color(255, 255, 255));
        BtnCreate.setText("Registrar");
        BtnCreate.setBorder(null);
        BtnCreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCreateActionPerformed(evt);
            }
        });
        BgPanel.add(BtnCreate, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 120, 110, 40));

        AppMenu.setBackground(new java.awt.Color(240, 240, 240));
        AppMenu.setBorder(null);
        AppMenu.setBorderPainted(false);

        Organization.setText("Registrar");

        Agent.setText("Agente");
        Agent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AgentActionPerformed(evt);
            }
        });
        Organization.add(Agent);

        Delinquent.setText("Delincuente");
        Delinquent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DelinquentActionPerformed(evt);
            }
        });
        Organization.add(Delinquent);

        DelinquentCharge.setText("Organización");
        DelinquentCharge.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DelinquentChargeActionPerformed(evt);
            }
        });
        Organization.add(DelinquentCharge);

        AppMenu.add(Organization);

        Reports.setText("Reportes");
        Reports.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ReportsMouseClicked(evt);
            }
        });
        Reports.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ReportsActionPerformed(evt);
            }
        });
        AppMenu.add(Reports);

        setJMenuBar(AppMenu);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(BgPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 350, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(BgPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void TxtNameKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtNameKeyReleased
        this.validateForm();
    }//GEN-LAST:event_TxtNameKeyReleased

    private void BtnCreateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCreateActionPerformed
        var success = controller.registerOrganization(TxtName.getText().trim());
        if (success) {
            JOptionPane.showMessageDialog(this, "Organización Creado con Exito");
            Kangaroo.goTo(MainFrame.MainController.class);
        } else  {
            JOptionPane.showMessageDialog(this, "ALGO HA SALIDO MAL!");
        }
    }//GEN-LAST:event_BtnCreateActionPerformed

    private void AgentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AgentActionPerformed
        Kangaroo.goTo(AgentsController.class);
    }//GEN-LAST:event_AgentActionPerformed

    private void DelinquentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DelinquentActionPerformed
        Kangaroo.goTo(DelinquentsController.class);
    }//GEN-LAST:event_DelinquentActionPerformed

    private void DelinquentChargeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DelinquentChargeActionPerformed
        Kangaroo.goTo(OrganizationsController.class);
    }//GEN-LAST:event_DelinquentChargeActionPerformed

    private void ReportsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ReportsMouseClicked
        Kangaroo.goTo(ReportsController.class);
    }//GEN-LAST:event_ReportsMouseClicked

    private void ReportsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ReportsActionPerformed
        Kangaroo.goTo(ReportsController.class);
    }//GEN-LAST:event_ReportsActionPerformed

    public void validateForm() {
        if (this.organizations.contains(TxtName.getText().trim())
            || TxtName.getText().isBlank()
        ) {
            BtnCreate.setEnabled(false);
            BtnCreate.setBackground(Color.LIGHT_GRAY);
            LblName.setForeground(Color.RED);
        } else {
            BtnCreate.setEnabled(true);
            BtnCreate.setBackground(this.BtnCreateColor);
            LblName.setForeground(Color.BLACK);
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem Agent;
    private javax.swing.JMenuBar AppMenu;
    private javax.swing.JPanel BgPanel;
    private javax.swing.JButton BtnCreate;
    private javax.swing.JMenuItem Delinquent;
    private javax.swing.JMenuItem DelinquentCharge;
    private javax.swing.JLabel LblName;
    private javax.swing.JLabel LblTitle;
    private javax.swing.JMenu Organization;
    private javax.swing.JMenu Reports;
    private javax.swing.JTextField TxtName;
    // End of variables declaration//GEN-END:variables

}
