package App.Frames;

import App.Controllers.Forms.AgentsController;
import App.Controllers.Forms.DelinquentsController;
import App.Controllers.Forms.OrganizationsController;
import App.Controllers.ReportsController;
import FBMY.navigation.Controller;
import FBMY.navigation.Kangaroo;
import FBMY.navigation.Frame;

public class MainFrame extends Frame<MainFrame.MainController> {

    public static class MainController extends Controller<MainFrame> {
        public MainController(MainFrame frame) { super(frame); frame.setVisible(true); }
    } 
    
    public MainFrame() {
        initComponents();
    }

    @Override
    public void setUp() {
       this.setSize(400, 240);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        PnlBg = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        AppMenu = new javax.swing.JMenuBar();
        Organization = new javax.swing.JMenu();
        Agent = new javax.swing.JMenuItem();
        Delinquent = new javax.swing.JMenuItem();
        DelinquentCharge = new javax.swing.JMenuItem();
        Reports = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        PnlBg.setBackground(new java.awt.Color(255, 255, 255));
        PnlBg.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Laboratorio de Delinquentes");
        PnlBg.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 50, 400, 50));

        jLabel2.setBackground(new java.awt.Color(204, 204, 204));
        jLabel2.setForeground(new java.awt.Color(204, 204, 204));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("UTN 2023 PII");
        PnlBg.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 120, 380, 20));

        jLabel3.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("Minor Yorseth Retana Vásquez");
        PnlBg.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 90, 400, 30));

        AppMenu.setBackground(new java.awt.Color(240, 240, 240));
        AppMenu.setBorder(null);
        AppMenu.setBorderPainted(false);

        Organization.setText("Registrar");

        Agent.setText("Agente");
        Agent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AgentActionPerformed(evt);
            }
        });
        Organization.add(Agent);

        Delinquent.setText("Delincuente");
        Delinquent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DelinquentActionPerformed(evt);
            }
        });
        Organization.add(Delinquent);

        DelinquentCharge.setText("Organización");
        DelinquentCharge.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DelinquentChargeActionPerformed(evt);
            }
        });
        Organization.add(DelinquentCharge);

        AppMenu.add(Organization);

        Reports.setText("Reportes");
        Reports.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ReportsMouseClicked(evt);
            }
        });
        Reports.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ReportsActionPerformed(evt);
            }
        });
        AppMenu.add(Reports);

        setJMenuBar(AppMenu);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(PnlBg, javax.swing.GroupLayout.PREFERRED_SIZE, 400, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(PnlBg, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void AgentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AgentActionPerformed
        Kangaroo.goTo(AgentsController.class);
    }//GEN-LAST:event_AgentActionPerformed

    private void DelinquentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DelinquentActionPerformed
        Kangaroo.goTo(DelinquentsController.class);
    }//GEN-LAST:event_DelinquentActionPerformed

    private void DelinquentChargeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DelinquentChargeActionPerformed
        Kangaroo.goTo(OrganizationsController.class);
    }//GEN-LAST:event_DelinquentChargeActionPerformed

    private void ReportsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ReportsActionPerformed
        Kangaroo.goTo(ReportsController.class);
    }//GEN-LAST:event_ReportsActionPerformed

    private void ReportsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ReportsMouseClicked
        Kangaroo.goTo(ReportsController.class);
    }//GEN-LAST:event_ReportsMouseClicked

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem Agent;
    private javax.swing.JMenuBar AppMenu;
    private javax.swing.JMenuItem Delinquent;
    private javax.swing.JMenuItem DelinquentCharge;
    private javax.swing.JMenu Organization;
    private javax.swing.JPanel PnlBg;
    private javax.swing.JMenu Reports;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    // End of variables declaration//GEN-END:variables
}
