package App.Frames;

import App.Controllers.Forms.AgentsController;
import App.Controllers.Forms.DelinquentsController;
import App.Controllers.Forms.OrganizationsController;
import App.Controllers.ReportsController;
import FBMY.navigation.Frame;
import App.Models.IdAndName;
import FBMY.navigation.Kangaroo;
import java.awt.BorderLayout;
import java.util.List;
import javax.swing.table.DefaultTableModel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.data.general.DefaultPieDataset;

public class ReportsFrame extends Frame<ReportsController> {

    List<IdAndName> agents;
    
    public ReportsFrame() {
        initComponents();
    }

    @Override
    public void setUp() {
        this.setSize(1050, 420);
        this.agents = controller.getAgents();
        this.agents.forEach((a) -> {
            CbxAgent.addItem(a.name());
        });
        
        var chargesData = controller.getChargesCounts();
        var dataset = new DefaultPieDataset();
        for (var row : chargesData)
            dataset.setValue(row.name(), row.id());
        var chart = ChartFactory.createPieChart("Conteo de Cargos", dataset);
        PnlChart.removeAll();
        PnlChart.add(new ChartPanel(chart), BorderLayout.CENTER);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        BgPanel = new javax.swing.JPanel();
        LblTitle = new javax.swing.JLabel();
        LblCountry = new javax.swing.JLabel();
        CbxAgent = new javax.swing.JComboBox<>();
        ScrDelinquents = new javax.swing.JScrollPane();
        TblDelinquents = new javax.swing.JTable();
        PnlChart = new javax.swing.JPanel();
        AppMenu = new javax.swing.JMenuBar();
        Organization = new javax.swing.JMenu();
        Agent = new javax.swing.JMenuItem();
        Delinquent = new javax.swing.JMenuItem();
        DelinquentCharge = new javax.swing.JMenuItem();
        Reports = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        BgPanel.setBackground(new java.awt.Color(255, 255, 255));
        BgPanel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        LblTitle.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        LblTitle.setText("Nuevo Delincuente");
        BgPanel.add(LblTitle, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, 230, -1));

        LblCountry.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        LblCountry.setText("Agente:");
        BgPanel.add(LblCountry, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 70, 90, 30));

        CbxAgent.setBackground(new java.awt.Color(255, 255, 254));
        CbxAgent.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        CbxAgent.setBorder(null);
        CbxAgent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CbxAgentActionPerformed(evt);
            }
        });
        BgPanel.add(CbxAgent, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 70, 200, -1));

        TblDelinquents.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        TblDelinquents.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Alias", "Nombre", "Cargos", "Organización"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        TblDelinquents.setRowHeight(25);
        ScrDelinquents.setViewportView(TblDelinquents);

        BgPanel.add(ScrDelinquents, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 120, 590, 260));

        PnlChart.setLayout(new java.awt.BorderLayout());
        BgPanel.add(PnlChart, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 20, 400, 360));

        AppMenu.setBackground(new java.awt.Color(240, 240, 240));
        AppMenu.setBorder(null);
        AppMenu.setBorderPainted(false);

        Organization.setText("Registrar");

        Agent.setText("Agente");
        Agent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AgentActionPerformed(evt);
            }
        });
        Organization.add(Agent);

        Delinquent.setText("Delincuente");
        Delinquent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DelinquentActionPerformed(evt);
            }
        });
        Organization.add(Delinquent);

        DelinquentCharge.setText("Organización");
        DelinquentCharge.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DelinquentChargeActionPerformed(evt);
            }
        });
        Organization.add(DelinquentCharge);

        AppMenu.add(Organization);

        Reports.setText("Reportes");
        Reports.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ReportsMouseClicked(evt);
            }
        });
        Reports.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ReportsActionPerformed(evt);
            }
        });
        AppMenu.add(Reports);

        setJMenuBar(AppMenu);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(BgPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 1050, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(BgPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 400, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void CbxAgentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CbxAgentActionPerformed
        this.fillTable();
    }//GEN-LAST:event_CbxAgentActionPerformed

    private void AgentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AgentActionPerformed
        Kangaroo.goTo(AgentsController.class);
    }//GEN-LAST:event_AgentActionPerformed

    private void DelinquentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DelinquentActionPerformed
        Kangaroo.goTo(DelinquentsController.class);
    }//GEN-LAST:event_DelinquentActionPerformed

    private void DelinquentChargeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DelinquentChargeActionPerformed
        Kangaroo.goTo(OrganizationsController.class);
    }//GEN-LAST:event_DelinquentChargeActionPerformed

    private void ReportsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ReportsMouseClicked
        Kangaroo.goTo(ReportsController.class);
    }//GEN-LAST:event_ReportsMouseClicked

    private void ReportsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ReportsActionPerformed
        Kangaroo.goTo(ReportsController.class);
    }//GEN-LAST:event_ReportsActionPerformed

    public void fillTable() {
        var agent = agents.get(CbxAgent.getSelectedIndex());
        var data = controller.getDelinquentsByAgent(agent.id());
        
        var model = (DefaultTableModel) TblDelinquents.getModel();
        model.setRowCount(0);
        data.forEach((row) -> {
            model.addRow(new Object[]{
                row.alias(),
                row.full_name(),
                row.charges(),
                row.organization(),
                row.agent(),
            });
        });
    
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem Agent;
    private javax.swing.JMenuBar AppMenu;
    private javax.swing.JPanel BgPanel;
    private javax.swing.JComboBox<String> CbxAgent;
    private javax.swing.JMenuItem Delinquent;
    private javax.swing.JMenuItem DelinquentCharge;
    private javax.swing.JLabel LblCountry;
    private javax.swing.JLabel LblTitle;
    private javax.swing.JMenu Organization;
    private javax.swing.JPanel PnlChart;
    private javax.swing.JMenu Reports;
    private javax.swing.JScrollPane ScrDelinquents;
    private javax.swing.JTable TblDelinquents;
    // End of variables declaration//GEN-END:variables
}
