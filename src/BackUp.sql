--
-- PostgreSQL database dump
--

-- Dumped from database version 15.2
-- Dumped by pg_dump version 15.2

-- Started on 2023-03-21 07:21:14

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 4 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: pg_database_owner
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO pg_database_owner;

--
-- TOC entry 3383 (class 0 OID 0)
-- Dependencies: 4
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: pg_database_owner
--

COMMENT ON SCHEMA public IS 'standard public schema';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 215 (class 1259 OID 25939)
-- Name: agents; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.agents (
    id integer NOT NULL,
    name character varying(50),
    surname character varying(50),
    cellphone character varying(50),
    created_at timestamp without time zone DEFAULT now()
);


ALTER TABLE public.agents OWNER TO postgres;

--
-- TOC entry 214 (class 1259 OID 25938)
-- Name: agents_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.agents_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agents_id_seq OWNER TO postgres;

--
-- TOC entry 3384 (class 0 OID 0)
-- Dependencies: 214
-- Name: agents_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.agents_id_seq OWNED BY public.agents.id;


--
-- TOC entry 219 (class 1259 OID 25958)
-- Name: charges; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.charges (
    id integer NOT NULL,
    name character varying(50)
);


ALTER TABLE public.charges OWNER TO postgres;

--
-- TOC entry 218 (class 1259 OID 25957)
-- Name: charges_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.charges_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.charges_id_seq OWNER TO postgres;

--
-- TOC entry 3385 (class 0 OID 0)
-- Dependencies: 218
-- Name: charges_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.charges_id_seq OWNED BY public.charges.id;


--
-- TOC entry 224 (class 1259 OID 26000)
-- Name: countries; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.countries (
    name character varying(50)
);


ALTER TABLE public.countries OWNER TO postgres;

--
-- TOC entry 221 (class 1259 OID 25965)
-- Name: delinquents; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.delinquents (
    id integer NOT NULL,
    name character varying(50),
    surname character varying(50),
    alias character varying(50),
    country character varying(50),
    reward integer,
    birth_date timestamp without time zone,
    agent_id integer,
    organization_id integer,
    created_at timestamp without time zone DEFAULT now()
);


ALTER TABLE public.delinquents OWNER TO postgres;

--
-- TOC entry 223 (class 1259 OID 25983)
-- Name: delinquents_charges; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.delinquents_charges (
    id integer NOT NULL,
    delinquent_id integer,
    charge_id integer,
    victims integer,
    created_at timestamp without time zone DEFAULT now()
);


ALTER TABLE public.delinquents_charges OWNER TO postgres;

--
-- TOC entry 222 (class 1259 OID 25982)
-- Name: delinquents_charges_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.delinquents_charges_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.delinquents_charges_id_seq OWNER TO postgres;

--
-- TOC entry 3386 (class 0 OID 0)
-- Dependencies: 222
-- Name: delinquents_charges_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.delinquents_charges_id_seq OWNED BY public.delinquents_charges.id;


--
-- TOC entry 220 (class 1259 OID 25964)
-- Name: delinquents_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.delinquents_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.delinquents_id_seq OWNER TO postgres;

--
-- TOC entry 3387 (class 0 OID 0)
-- Dependencies: 220
-- Name: delinquents_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.delinquents_id_seq OWNED BY public.delinquents.id;


--
-- TOC entry 217 (class 1259 OID 25949)
-- Name: organizations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.organizations (
    id integer NOT NULL,
    name character varying(50)
);


ALTER TABLE public.organizations OWNER TO postgres;

--
-- TOC entry 216 (class 1259 OID 25948)
-- Name: organizations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.organizations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.organizations_id_seq OWNER TO postgres;

--
-- TOC entry 3388 (class 0 OID 0)
-- Dependencies: 216
-- Name: organizations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.organizations_id_seq OWNED BY public.organizations.id;


--
-- TOC entry 3197 (class 2604 OID 25942)
-- Name: agents id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.agents ALTER COLUMN id SET DEFAULT nextval('public.agents_id_seq'::regclass);


--
-- TOC entry 3200 (class 2604 OID 25961)
-- Name: charges id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.charges ALTER COLUMN id SET DEFAULT nextval('public.charges_id_seq'::regclass);


--
-- TOC entry 3201 (class 2604 OID 25968)
-- Name: delinquents id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.delinquents ALTER COLUMN id SET DEFAULT nextval('public.delinquents_id_seq'::regclass);


--
-- TOC entry 3203 (class 2604 OID 25986)
-- Name: delinquents_charges id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.delinquents_charges ALTER COLUMN id SET DEFAULT nextval('public.delinquents_charges_id_seq'::regclass);


--
-- TOC entry 3199 (class 2604 OID 25952)
-- Name: organizations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.organizations ALTER COLUMN id SET DEFAULT nextval('public.organizations_id_seq'::regclass);


--
-- TOC entry 3368 (class 0 OID 25939)
-- Dependencies: 215
-- Data for Name: agents; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.agents (id, name, surname, cellphone, created_at) FROM stdin;
1	Marlon	Aufderhar	1-905-657-5809 x916	2023-03-21 06:31:53.244507
2	Cleo	Schinner	995-510-4134 x7672	2023-03-21 06:31:53.244507
3	Kaelyn	Gutkowski	939-474-8551	2023-03-21 06:31:53.244507
4	Christian	Hintz	670.526.0792 x93541	2023-03-21 06:31:53.244507
5	Rosalee	Mohr	(827) 941-7696 x182	2023-03-21 06:31:53.244507
6	Green	Klocko	779.771.8199 x2980	2023-03-21 06:31:53.244507
7	Willow	Wisoky	352.721.2402 x611	2023-03-21 06:31:53.244507
8	Benjamin	Skiles	(463) 926-2659 x02083	2023-03-21 06:31:53.244507
9	Karine	Gusikowski	1-498-240-2834 x246	2023-03-21 06:31:53.244507
10	Palma	Mayert	666.623.8266 x7717	2023-03-21 06:31:53.244507
11	Adrian	Herman	1-509-844-6403 x05089	2023-03-21 06:31:53.244507
12	Xander	Bogisich	1-338-859-0030	2023-03-21 06:31:53.244507
13	Lauryn	Roberts	1-409-365-5247 x255	2023-03-21 06:31:53.244507
14	Elliot	Rice	(799) 742-7799 x9278	2023-03-21 06:31:53.244507
15	Robert	Waters	530-791-8833	2023-03-21 06:31:53.244507
16	Mac	Quigley	1-266-774-2793 x4910	2023-03-21 06:31:53.244507
17	Therese	Ankunding	661-485-7411 x6401	2023-03-21 06:31:53.244507
18	Melody	Morar	330.631.5545 x6732	2023-03-21 06:31:53.244507
19	Reanna	Bosco	(257) 425-7650 x659	2023-03-21 06:31:53.244507
20	Dasia	Klocko	588-428-5467 x6679	2023-03-21 06:31:53.244507
21	Maggie	Senger	448.640.5085	2023-03-21 06:31:53.244507
22	Markus	Williamson	(339) 749-1435 x69545	2023-03-21 06:31:53.244507
23	Alek	Labadie	(709) 227-1964 x886	2023-03-21 06:31:53.244507
24	Kyra	Toy	239-249-1607 x784	2023-03-21 06:31:53.244507
25	Jose	Bartell	875.362.5580 x838	2023-03-21 06:31:53.244507
26	Jorge	Greenholt	1-280-594-4152 x363	2023-03-21 06:31:53.244507
27	Kelton	Yundt	1-811-517-9839 x2620	2023-03-21 06:31:53.244507
28	Griffin	Kshlerin	1-895-252-8879	2023-03-21 06:31:53.244507
29	Waylon	Jast	1-626-630-4514 x20926	2023-03-21 06:31:53.244507
30	Deron	Bradtke	(363) 355-1919 x36789	2023-03-21 06:31:53.244507
31	Daniela	Torphy	1-640-916-6394 x381	2023-03-21 06:31:53.244507
32	Vesta	Feest	775.705.9509 x28727	2023-03-21 06:31:53.244507
33	Roosevelt	Rutherford	(370) 245-9486 x4184	2023-03-21 06:31:53.244507
34	Darrion	Koch	273.568.9835 x67275	2023-03-21 06:31:53.244507
35	Jackeline	Hoeger	588.874.1418 x9086	2023-03-21 06:31:53.244507
36	Lora	Purdy	434-739-7105 x33140	2023-03-21 06:31:53.244507
37	Ian	Klocko	688-263-4808 x9217	2023-03-21 06:31:53.244507
38	Tad	Boyer	1-207-683-2555	2023-03-21 06:31:53.244507
39	Estefania	Koelpin	442.978.8662	2023-03-21 06:31:53.244507
40	Ed	Von	629-216-6426	2023-03-21 06:31:53.244507
41	Minor Yorseth	Retana Vásquez	+506 8304-6129	2023-03-21 07:17:33.813337
\.


--
-- TOC entry 3372 (class 0 OID 25958)
-- Dependencies: 219
-- Data for Name: charges; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.charges (id, name) FROM stdin;
1	Asesinato
2	Asalto
3	Extorción
4	Estafas
5	Alborotos
6	Violencia
7	Secuestro
8	Violación
9	Crímen Menor
10	Vandalismo
11	Compra y Venta de Drogas
12	Carterista
\.


--
-- TOC entry 3377 (class 0 OID 26000)
-- Dependencies: 224
-- Data for Name: countries; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.countries (name) FROM stdin;
Afganistán
Albania
Alemania
Andorra
Angola
Antigua y Barbuda
Arabia Saudita
Argelia
Argentina
Armenia
Australia
Austria
Azerbaiyán
Bahamas
Bahrein
Bangladesh
Barbados
Belarús
Bélgica
Belice
Benin
Bhután
Bolivia
Bosnia y Herzegovina
Botswana
Brasil
Brunei Darussalam
Bulgaria
Burkina Faso
Burundi
Cabo Verde
Camboya
Camerún
Canadá
Chad
Chequia
Chile
China
Chipre
Colombia
Comoras
Congo
Costa Rica
Côte d´Ivoire
Croacia
Cuba
Dinamarca
Djibouti
Dominica
Ecuador
Egipto
El Salvador
Emiratos Árabes Unidos
Eritrea
Eslovaquia
Eslovenia
España
Estados Unidos de América
Estonia
Eswatini
Etiopía
Federación de Rusia
Fiji
Filipinas
Finlandia
Francia
Gabón
Gambia
Georgia
Ghana
Granada
Grecia
Guatemala
Guinea
Guinea Ecuatorial
Guinea-Bissau
Guyana
Haití
Honduras
Hungría
India
Indonesia
Irán
Iraq
Irlanda
Islandia
Islas Cook
Islas Marshall
Islas Salomón
Israel
Italia
Jamaica
Japón
Jordania
Kazajstán
Kenya
Kirguistán
Kiribati
Kuwait
Lesotho
Letonia
Líbano
Liberia
Libia
Liechtenstein
Lituania
Luxemburgo
Madagascar
Malasia
Malawi
Maldivas
Malí
Malta
Marruecos
Mauricio
Mauritania
México
Micronesia
Mónaco
Mongolia
Montenegro
Mozambique
Myanmar
Namibia
Nauru
Nepal
Nicaragua
Níger
Nigeria
Niue
Noruega
Nueva Zelandia
Omán
Países Bajos
Pakistán
Palau
Panamá
Papua Nueva Guinea
Paraguay
Perú
Polonia
Portugal
Qatar
Reino Unido de Gran Bretaña e Irlanda del Norte
República Árabe Siria
República Centroafricana
República de Corea
República de Macedonia del Norte
Nombre corto: Macedonia del Norte
República de Moldova
República Democrática del Congo
República Democrática Popular Lao
República Dominicana
República Popular Democrática de Corea
República Unida de Tanzanía
Rumania
Rwanda
Saint Kitts y Nevis
Samoa
San Marino
San Vicente y las Granadinas
Santa Lucía
Santa Sede
Santo Tomé y Príncipe
Senegal
Serbia
Seychelles
Sierra Leona
Singapur
Somalia
Sri Lanka
Sudáfrica
Sudán
Sudán del Sur
Suecia
Suiza
Suriname
Tailandia
Tayikistán
Timor-Leste
Togo
Tonga
Trinidad y Tabago
Túnez
Türkiye
Turkmenistán
Tuvalu
Ucrania
Uganda
Uruguay
Uzbekistán
Vanuatu
Venezuela
Viet Nam
Yemen
Zambia
Zimbabwe
\.


--
-- TOC entry 3374 (class 0 OID 25965)
-- Dependencies: 221
-- Data for Name: delinquents; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.delinquents (id, name, surname, alias, country, reward, birth_date, agent_id, organization_id, created_at) FROM stdin;
1	Abel	Bednar	Dolores.Pfeffer4	Slovenia	16000	2023-02-01 00:00:00	12	5	2023-03-21 06:31:53.358774
2	Abigale	Wolf	Kay_Schinner96	Guam	21000	2022-06-01 00:00:00	20	4	2023-03-21 06:31:53.358774
3	Adele	Lindgren	Tina59	Ireland	68000	2022-08-29 00:00:00	8	3	2023-03-21 06:31:53.358774
4	Adell	Renner	Dixie89	Syrian Arab Republic	11000	2022-06-20 00:00:00	37	2	2023-03-21 06:31:53.358774
5	Aditya	Kozey	Wilmer.Crist67	South Georgia and the South Sandwich Islands	14000	2023-01-20 00:00:00	25	2	2023-03-21 06:31:53.358774
6	Ally	Kozey	Jazmyn86	Nauru	12000	2022-08-04 00:00:00	20	5	2023-03-21 06:31:53.358774
7	Amaya	Nikolaus	Floy87	Bahamas	77000	2022-10-02 00:00:00	9	3	2023-03-21 06:31:53.358774
8	Amely	Beer	Tyrique.Schuster81	Switzerland	84000	2022-08-19 00:00:00	14	1	2023-03-21 06:31:53.358774
9	Amina	Batz	Karli_Johnson21	Cameroon	67000	2022-11-04 00:00:00	37	3	2023-03-21 06:31:53.358774
10	Aniyah	Jaskolski	Eda_Reichert73	Antarctica (the territory South of 60 deg S)	29000	2022-04-03 00:00:00	21	1	2023-03-21 06:31:53.358774
11	Annabell	Bahringer	Cordelia_Smith63	Namibia	58000	2023-03-03 00:00:00	7	2	2023-03-21 06:31:53.358774
12	Annabelle	O`Conner	Nathanial.Leannon53	Peru	68000	2022-04-11 00:00:00	10	1	2023-03-21 06:31:53.358774
13	Annette	Langosh	Kris.Cartwright	Estonia	37000	2022-05-21 00:00:00	24	1	2023-03-21 06:31:53.358774
14	Anya	Quigley	Deven_Harber	Ukraine	44000	2022-10-15 00:00:00	11	2	2023-03-21 06:31:53.358774
15	Arely	Willms	Marge69	Mozambique	45000	2022-11-11 00:00:00	17	1	2023-03-21 06:31:53.358774
16	Aubrey	Christiansen	Sophie_Kertzmann46	Christmas Island	42000	2022-09-18 00:00:00	9	4	2023-03-21 06:31:53.358774
17	Austyn	Reynolds	Ruth_Larson	Bouvet Island (Bouvetoya)	73000	2022-09-12 00:00:00	18	3	2023-03-21 06:31:53.358774
18	Ayden	Grady	Opal29	Iceland	18000	2023-02-16 00:00:00	12	2	2023-03-21 06:31:53.358774
19	Baylee	Jenkins	Rey15	Tokelau	10000	2022-12-23 00:00:00	30	5	2023-03-21 06:31:53.358774
20	Bridget	Parisian	Nyah31	Tonga	80000	2023-02-24 00:00:00	14	1	2023-03-21 06:31:53.358774
21	Brielle	Bailey	Ora17	Luxembourg	13000	2022-04-29 00:00:00	1	1	2023-03-21 06:31:53.358774
22	Brock	Stracke	Lorenz71	Moldova	98000	2022-05-20 00:00:00	12	3	2023-03-21 06:31:53.358774
23	Burnice	Maggio	Elyssa.Feeney88	Czech Republic	58000	2022-09-22 00:00:00	29	2	2023-03-21 06:31:53.358774
24	Callie	Schroeder	Gilbert18	Slovakia (Slovak Republic)	92000	2022-06-26 00:00:00	25	1	2023-03-21 06:31:53.358774
25	Carli	Howell	Sophia43	Cote d`Ivoire	65000	2022-06-22 00:00:00	19	5	2023-03-21 06:31:53.358774
26	Carmine	Murazik	Alford.Hettinger	Finland	75000	2023-03-05 00:00:00	5	1	2023-03-21 06:31:53.358774
27	Cassandre	Glover	Orie_Farrell24	Maldives	80000	2022-10-05 00:00:00	6	1	2023-03-21 06:31:53.358774
28	Chandler	Jacobi	Arden57	Somalia	72000	2022-06-12 00:00:00	40	2	2023-03-21 06:31:53.358774
29	Chesley	Dickinson	Lilliana_Skiles78	Martinique	94000	2022-12-19 00:00:00	4	2	2023-03-21 06:31:53.358774
30	Citlalli	Stanton	Jamal70	Mongolia	96000	2023-03-09 00:00:00	4	4	2023-03-21 06:31:53.358774
31	Cleta	Klein	Kimberly_Dibbert91	Bangladesh	21000	2022-07-23 00:00:00	21	1	2023-03-21 06:31:53.358774
32	Clifford	Ziemann	Elza47	Georgia	38000	2023-02-25 00:00:00	30	4	2023-03-21 06:31:53.358774
33	Cortez	Schinner	Ruben.Lehner1	France	23000	2022-07-11 00:00:00	31	1	2023-03-21 06:31:53.358774
34	Cynthia	Emard	Skylar_Effertz97	Guernsey	42000	2022-08-02 00:00:00	30	1	2023-03-21 06:31:53.358774
35	Daisha	McCullough	Stan.OKon25	Kazakhstan	56000	2022-09-02 00:00:00	22	5	2023-03-21 06:31:53.358774
36	Daisy	Reilly	Meda88	Saint Vincent and the Grenadines	49000	2022-07-18 00:00:00	39	5	2023-03-21 06:31:53.358774
37	Damien	Bailey	Carmella.Buckridge	Cook Islands	38000	2022-10-22 00:00:00	27	5	2023-03-21 06:31:53.358774
38	Darrion	Grant	Terence.Streich27	Latvia	87000	2022-08-24 00:00:00	17	5	2023-03-21 06:31:53.358774
39	Dedrick	Wuckert	Petra.Lockman54	Martinique	100000	2022-04-26 00:00:00	36	5	2023-03-21 06:31:53.358774
40	Dejuan	Trantow	Neva_Willms	Italy	16000	2023-02-11 00:00:00	5	4	2023-03-21 06:31:53.358774
41	Deondre	Feeney	Pierre12	Iraq	95000	2022-11-18 00:00:00	8	4	2023-03-21 06:31:53.358774
42	Deron	Deckow	Derrick46	Hungary	78000	2022-09-05 00:00:00	12	4	2023-03-21 06:31:53.358774
43	Desmond	Kutch	Jess26	Samoa	39000	2022-05-24 00:00:00	38	4	2023-03-21 06:31:53.358774
44	Destinee	Frami	Sasha5	Indonesia	98000	2022-08-05 00:00:00	1	3	2023-03-21 06:31:53.358774
45	Dillan	Cruickshank	Terrance_Nolan	Ireland	85000	2022-10-04 00:00:00	28	3	2023-03-21 06:31:53.358774
46	Douglas	MacGyver	Berta92	Tunisia	67000	2022-09-25 00:00:00	3	5	2023-03-21 06:31:53.358774
47	Dusty	Schmeler	Marina_Beer33	Cote d`Ivoire	33000	2022-07-21 00:00:00	2	2	2023-03-21 06:31:53.358774
48	Earl	Ondricka	Toni.Collier	Solomon Islands	40000	2023-01-06 00:00:00	18	1	2023-03-21 06:31:53.358774
49	Ed	Hudson	Gaston82	Germany	39000	2022-10-20 00:00:00	10	1	2023-03-21 06:31:53.358774
50	Efren	Gusikowski	Elizabeth_Hartmann33	Morocco	50000	2023-02-13 00:00:00	21	2	2023-03-21 06:31:53.358774
51	Ellis	Lebsack	Clinton.McClure	Greece	91000	2022-05-27 00:00:00	9	1	2023-03-21 06:31:53.358774
52	Elroy	Dicki	Timmothy_Volkman9	Tonga	51000	2022-11-07 00:00:00	16	5	2023-03-21 06:31:53.358774
53	Elvis	Lowe	Elisabeth66	Heard Island and McDonald Islands	54000	2022-07-24 00:00:00	26	3	2023-03-21 06:31:53.358774
54	Esteban	Kassulke	Keanu9	Guatemala	97000	2022-12-21 00:00:00	36	5	2023-03-21 06:31:53.358774
55	Ethan	White	Joaquin88	Kiribati	15000	2023-03-01 00:00:00	15	5	2023-03-21 06:31:53.358774
56	Eva	Blanda	Columbus77	United States Minor Outlying Islands	11000	2022-07-26 00:00:00	13	2	2023-03-21 06:31:53.358774
57	Evelyn	Hansen	Amely16	Kenya	61000	2022-07-09 00:00:00	21	2	2023-03-21 06:31:53.358774
58	Filiberto	Boyle	Tressa.Sawayn	Saint Pierre and Miquelon	95000	2022-12-25 00:00:00	35	2	2023-03-21 06:31:53.358774
59	George	Shields	Darlene.Mayer	Swaziland	89000	2022-06-13 00:00:00	27	1	2023-03-21 06:31:53.358774
60	Gerson	Vandervort	Enrico.Vandervort46	Saint Kitts and Nevis	15000	2023-01-04 00:00:00	24	3	2023-03-21 06:31:53.358774
61	Glenda	Hansen	Murl_Jacobi	Pitcairn Islands	91000	2022-11-21 00:00:00	6	4	2023-03-21 06:31:53.358774
62	Godfrey	Koelpin	Rhett.Balistreri39	Mexico	59000	2023-01-28 00:00:00	9	3	2023-03-21 06:31:53.358774
63	Gwen	Collins	Santino.Champlin53	Senegal	60000	2022-12-29 00:00:00	13	1	2023-03-21 06:31:53.358774
64	Hadley	Rippin	Isadore_Boyer	Equatorial Guinea	67000	2022-05-15 00:00:00	16	5	2023-03-21 06:31:53.358774
65	Hallie	Mayert	Tess.Herzog	Jersey	83000	2022-07-01 00:00:00	5	2	2023-03-21 06:31:53.358774
66	Harmon	Muller	Neil87	Falkland Islands (Malvinas)	35000	2022-11-25 00:00:00	11	2	2023-03-21 06:31:53.358774
67	Hoyt	Jenkins	Toby32	Lesotho	79000	2022-12-12 00:00:00	14	4	2023-03-21 06:31:53.358774
68	Icie	Cassin	Katharina75	Bhutan	89000	2022-09-26 00:00:00	3	1	2023-03-21 06:31:53.358774
69	Jackson	Bernier	Dulce_Reinger95	Brazil	31000	2022-05-11 00:00:00	33	5	2023-03-21 06:31:53.358774
70	Jay	Ortiz	Conrad.Mayert41	Gabon	87000	2023-01-12 00:00:00	20	5	2023-03-21 06:31:53.358774
71	Jenifer	Monahan	Stacy.Herman	Honduras	63000	2023-03-13 00:00:00	25	1	2023-03-21 06:31:53.358774
72	Jerod	Walker	Barbara95	Vietnam	25000	2022-06-14 00:00:00	11	3	2023-03-21 06:31:53.358774
73	Jonatan	Wilkinson	Lucio.Wyman	Switzerland	83000	2022-05-04 00:00:00	14	1	2023-03-21 06:31:53.358774
74	Josiane	Kutch	Lonnie.Corwin	Myanmar	53000	2023-03-09 00:00:00	7	3	2023-03-21 06:31:53.358774
75	Kaelyn	Gutkowski	Godfrey_Reichel43	Vietnam	41000	2023-02-15 00:00:00	21	3	2023-03-21 06:31:53.358774
76	Kamryn	Johnston	Abigale_McKenzie0	Myanmar	39000	2022-10-17 00:00:00	26	1	2023-03-21 06:31:53.358774
77	Kassandra	Spencer	Verdie.Yundt18	Svalbard & Jan Mayen Islands	72000	2023-03-11 00:00:00	28	1	2023-03-21 06:31:53.358774
78	Katrine	Herzog	Price.Price	Cyprus	68000	2022-11-18 00:00:00	6	2	2023-03-21 06:31:53.358774
79	Kenneth	Heller	Ivory_Rippin91	Bouvet Island (Bouvetoya)	74000	2022-04-15 00:00:00	2	4	2023-03-21 06:31:53.358774
80	Kenny	Wilkinson	Ramiro74	Saint Helena	93000	2022-03-24 00:00:00	14	5	2023-03-21 06:31:53.358774
81	Kevon	Pollich	Cecil48	Cambodia	94000	2022-09-19 00:00:00	14	5	2023-03-21 06:31:53.358774
82	Kirsten	Leffler	Alvena.Hilll	Austria	73000	2022-12-05 00:00:00	29	2	2023-03-21 06:31:53.358774
83	Laurie	Schulist	Heber24	South Georgia and the South Sandwich Islands	70000	2022-06-05 00:00:00	31	5	2023-03-21 06:31:53.358774
84	Laury	Ward	Carmen.Beier32	India	100000	2022-04-13 00:00:00	33	1	2023-03-21 06:31:53.358774
85	Lavern	Harvey	Augusta_Parker98	French Southern Territories	46000	2023-02-23 00:00:00	38	5	2023-03-21 06:31:53.358774
86	Leanna	Zieme	Estevan25	Dominica	10000	2022-06-22 00:00:00	32	3	2023-03-21 06:31:53.358774
87	Leonora	Gerlach	Eda.Gutmann79	Puerto Rico	86000	2022-08-22 00:00:00	23	1	2023-03-21 06:31:53.358774
88	Liliana	Kiehn	Dillon79	Tanzania	69000	2022-09-10 00:00:00	23	3	2023-03-21 06:31:53.358774
89	Lina	Gulgowski	Jed.Bauch15	Cameroon	76000	2022-05-28 00:00:00	19	4	2023-03-21 06:31:53.358774
90	Lindsey	Spinka	Delaney.Russel	Jamaica	83000	2023-02-26 00:00:00	24	5	2023-03-21 06:31:53.358774
91	Lola	Collins	Chaya98	Belgium	24000	2022-04-27 00:00:00	18	2	2023-03-21 06:31:53.358774
92	Lydia	Hoppe	Bridget65	Isle of Man	40000	2022-04-08 00:00:00	3	2	2023-03-21 06:31:53.358774
93	Mackenzie	King	Drew_Kautzer82	Reunion	65000	2022-05-23 00:00:00	11	1	2023-03-21 06:31:53.358774
94	Madeline	Lowe	Brant89	Czech Republic	91000	2022-12-18 00:00:00	34	2	2023-03-21 06:31:53.358774
95	Major	Gerlach	Emmet_Collins	Ghana	99000	2022-07-26 00:00:00	31	5	2023-03-21 06:31:53.358774
96	Marge	Schuppe	Beverly34	Poland	14000	2022-04-17 00:00:00	34	1	2023-03-21 06:31:53.358774
97	Marie	Runte	Ray47	Singapore	64000	2023-01-25 00:00:00	8	3	2023-03-21 06:31:53.358774
98	Marisa	Cummings	Noelia.McGlynn24	Timor-Leste	80000	2023-01-25 00:00:00	22	1	2023-03-21 06:31:53.358774
99	Marjorie	Jast	Harold45	Sudan	62000	2022-12-08 00:00:00	5	2	2023-03-21 06:31:53.358774
100	Marlen	Pfannerstill	Maximillia.Mueller	Falkland Islands (Malvinas)	18000	2022-09-18 00:00:00	22	1	2023-03-21 06:31:53.358774
101	Marta	Schiller	Dayna81	Luxembourg	25000	2022-11-28 00:00:00	3	2	2023-03-21 06:31:53.358774
102	Mauricio	Considine	Anita.Predovic	Nigeria	20000	2022-06-22 00:00:00	19	5	2023-03-21 06:31:53.358774
103	Meghan	Hahn	Alexandrea43	Sierra Leone	31000	2023-02-01 00:00:00	24	1	2023-03-21 06:31:53.358774
104	Melyssa	Kovacek	Ora11	Nepal	43000	2022-08-31 00:00:00	37	2	2023-03-21 06:31:53.358774
105	Michel	Windler	Naomi.Swaniawski	San Marino	26000	2022-12-19 00:00:00	32	4	2023-03-21 06:31:53.358774
106	Michelle	Sipes	Bart45	Marshall Islands	41000	2022-07-27 00:00:00	8	5	2023-03-21 06:31:53.358774
107	Milton	Harvey	Georgianna.Rippin	Turkey	27000	2023-02-14 00:00:00	26	3	2023-03-21 06:31:53.358774
108	Miracle	Gleichner	Hyman38	Botswana	21000	2022-09-05 00:00:00	7	2	2023-03-21 06:31:53.358774
109	Mollie	Bosco	Helga74	Iraq	55000	2022-04-01 00:00:00	13	5	2023-03-21 06:31:53.358774
110	Molly	Olson	Jake_Wisozk78	Dominican Republic	86000	2022-08-15 00:00:00	26	2	2023-03-21 06:31:53.358774
111	Monica	Gibson	Braxton.Ritchie	Nepal	77000	2022-05-20 00:00:00	2	1	2023-03-21 06:31:53.358774
112	Muhammad	Larson	Ewell.Waelchi39	Australia	21000	2022-05-28 00:00:00	14	3	2023-03-21 06:31:53.358774
113	Nannie	Schultz	Juliet5	Belgium	11000	2023-01-15 00:00:00	19	1	2023-03-21 06:31:53.358774
114	Nedra	Monahan	Eunice43	Australia	87000	2022-06-12 00:00:00	21	2	2023-03-21 06:31:53.358774
115	Nola	Barton	Minnie.Mraz50	Georgia	45000	2022-12-26 00:00:00	9	1	2023-03-21 06:31:53.358774
116	Nora	O`Hara	Leslie_Abshire1	Burundi	96000	2022-12-06 00:00:00	4	4	2023-03-21 06:31:53.358774
117	Nya	Hoppe	Myrl_Heaney66	Djibouti	97000	2022-06-22 00:00:00	23	1	2023-03-21 06:31:53.358774
118	Odessa	Adams	Jaylen_Rodriguez	Sudan	52000	2023-01-10 00:00:00	21	5	2023-03-21 06:31:53.358774
119	Osbaldo	Ratke	Zoey15	Timor-Leste	59000	2022-07-18 00:00:00	18	1	2023-03-21 06:31:53.358774
120	Paul	Kirlin	Jeff.Bauch91	Macedonia	44000	2022-09-27 00:00:00	7	1	2023-03-21 06:31:53.358774
121	Pearlie	Robel	Trudie.Douglas	Azerbaijan	18000	2022-05-29 00:00:00	5	3	2023-03-21 06:31:53.358774
122	Reymundo	Sipes	Araceli.Feeney88	Belize	70000	2023-02-08 00:00:00	37	4	2023-03-21 06:31:53.358774
123	Robbie	Friesen	Alvera_Satterfield2	Belize	98000	2022-04-27 00:00:00	39	1	2023-03-21 06:31:53.358774
124	Rocio	Ryan	Karson69	Finland	18000	2022-04-23 00:00:00	18	3	2023-03-21 06:31:53.358774
125	Royce	Jacobson	Chandler.Cormier94	Costa Rica	32000	2022-11-25 00:00:00	8	1	2023-03-21 06:31:53.358774
126	Russel	Abshire	Delaney.Funk	Botswana	18000	2022-10-05 00:00:00	35	5	2023-03-21 06:31:53.358774
127	Ryleigh	Swaniawski	Esther.Kassulke48	Cameroon	80000	2022-12-09 00:00:00	6	4	2023-03-21 06:31:53.358774
128	Saige	Hagenes	Kirk43	Palestinian Territory	48000	2022-04-26 00:00:00	7	5	2023-03-21 06:31:53.358774
129	Sammie	Will	Kadin.Schneider	Bouvet Island (Bouvetoya)	91000	2022-10-18 00:00:00	7	3	2023-03-21 06:31:53.358774
130	Santina	Haag	Stewart38	Sri Lanka	10000	2022-07-11 00:00:00	12	3	2023-03-21 06:31:53.358774
131	Scarlett	Pouros	Leatha_Monahan	Uzbekistan	35000	2022-12-08 00:00:00	12	3	2023-03-21 06:31:53.358774
132	Schuyler	D`Amore	Salma.Kerluke58	Martinique	69000	2022-04-05 00:00:00	25	1	2023-03-21 06:31:53.358774
133	Shakira	Leannon	Freeman65	Algeria	26000	2022-11-24 00:00:00	1	5	2023-03-21 06:31:53.358774
134	Shanny	Oberbrunner	Enola.Shields17	Syrian Arab Republic	73000	2022-05-03 00:00:00	3	1	2023-03-21 06:31:53.358774
135	Shawna	Dach	Kennedy.Effertz89	Greece	37000	2022-04-07 00:00:00	27	5	2023-03-21 06:31:53.358774
136	Susana	Larkin	Haylee.Pouros62	Italy	100000	2023-01-12 00:00:00	35	1	2023-03-21 06:31:53.358774
137	Tanner	Conn	Danial11	Kyrgyz Republic	27000	2022-05-17 00:00:00	3	5	2023-03-21 06:31:53.358774
138	Tobin	Watsica	Domenick57	Uganda	99000	2023-01-22 00:00:00	29	5	2023-03-21 06:31:53.358774
139	Torrance	Kuhlman	Blaze_Witting	Palau	85000	2023-01-26 00:00:00	16	5	2023-03-21 06:31:53.358774
140	Ulices	Jerde	Ila_Friesen43	Canada	54000	2022-06-30 00:00:00	7	3	2023-03-21 06:31:53.358774
141	Vella	Kuhlman	Gladys.Grant	Mongolia	39000	2022-06-11 00:00:00	17	3	2023-03-21 06:31:53.358774
142	Vern	Fritsch	Koby_Toy51	Gambia	14000	2022-10-28 00:00:00	39	4	2023-03-21 06:31:53.358774
143	Vladimir	Hills	Keshawn54	Equatorial Guinea	89000	2022-11-09 00:00:00	17	1	2023-03-21 06:31:53.358774
144	Wanda	Douglas	Daphney70	Egypt	18000	2022-03-31 00:00:00	5	2	2023-03-21 06:31:53.358774
145	Wiley	Ward	Lela_Bernhard26	American Samoa	79000	2022-08-26 00:00:00	38	1	2023-03-21 06:31:53.358774
146	Wilhelm	Keeling	Mckenna21	Ecuador	19000	2022-04-22 00:00:00	36	1	2023-03-21 06:31:53.358774
147	Yessenia	Hickle	Helga32	Mayotte	59000	2022-10-11 00:00:00	9	2	2023-03-21 06:31:53.358774
148	Zachery	Ritchie	Alysson_Sporer87	Bahrain	99000	2022-12-27 00:00:00	35	4	2023-03-21 06:31:53.358774
149	Zackery	Larson	Andreane.Kub	Paraguay	14000	2022-08-04 00:00:00	16	5	2023-03-21 06:31:53.358774
150	Zaria	Langworth	Devante.Stehr75	Lao People`s Democratic Republic	31000	2022-09-02 00:00:00	8	2	2023-03-21 06:31:53.358774
151	Felipe	Aragon	Feragon	Costa Rica	122640	1200-01-01 00:00:00	41	4	2023-03-21 07:18:28.810775
\.


--
-- TOC entry 3376 (class 0 OID 25983)
-- Dependencies: 223
-- Data for Name: delinquents_charges; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.delinquents_charges (id, delinquent_id, charge_id, victims, created_at) FROM stdin;
1	39	2	462	2023-03-21 06:31:53.497274
2	16	11	764	2023-03-21 06:31:53.497274
3	112	9	743	2023-03-21 06:31:53.497274
4	66	11	684	2023-03-21 06:31:53.497274
5	58	3	245	2023-03-21 06:31:53.497274
6	96	8	80	2023-03-21 06:31:53.497274
7	16	9	357	2023-03-21 06:31:53.497274
8	139	4	421	2023-03-21 06:31:53.497274
9	137	9	645	2023-03-21 06:31:53.497274
10	60	3	295	2023-03-21 06:31:53.497274
11	141	11	408	2023-03-21 06:31:53.497274
12	86	5	80	2023-03-21 06:31:53.497274
13	80	8	833	2023-03-21 06:31:53.497274
14	47	10	269	2023-03-21 06:31:53.497274
15	136	1	753	2023-03-21 06:31:53.497274
16	13	9	444	2023-03-21 06:31:53.497274
17	53	10	19	2023-03-21 06:31:53.497274
18	11	3	62	2023-03-21 06:31:53.497274
19	127	9	623	2023-03-21 06:31:53.497274
20	15	5	878	2023-03-21 06:31:53.497274
21	146	7	163	2023-03-21 06:31:53.497274
22	118	2	768	2023-03-21 06:31:53.497274
23	85	5	13	2023-03-21 06:31:53.497274
24	80	10	208	2023-03-21 06:31:53.497274
25	110	9	984	2023-03-21 06:31:53.497274
26	45	4	737	2023-03-21 06:31:53.497274
27	54	4	157	2023-03-21 06:31:53.497274
28	118	5	957	2023-03-21 06:31:53.497274
29	129	7	260	2023-03-21 06:31:53.497274
30	6	7	273	2023-03-21 06:31:53.497274
31	28	3	230	2023-03-21 06:31:53.497274
32	1	1	817	2023-03-21 06:31:53.497274
33	127	4	357	2023-03-21 06:31:53.497274
34	95	5	783	2023-03-21 06:31:53.497274
35	87	5	433	2023-03-21 06:31:53.497274
36	81	8	849	2023-03-21 06:31:53.497274
37	38	11	556	2023-03-21 06:31:53.497274
38	103	8	66	2023-03-21 06:31:53.497274
39	128	5	184	2023-03-21 06:31:53.497274
40	68	1	386	2023-03-21 06:31:53.497274
41	35	2	128	2023-03-21 06:31:53.497274
42	14	4	488	2023-03-21 06:31:53.497274
43	79	2	385	2023-03-21 06:31:53.497274
44	82	6	161	2023-03-21 06:31:53.497274
45	115	6	589	2023-03-21 06:31:53.497274
46	67	6	101	2023-03-21 06:31:53.497274
47	90	7	521	2023-03-21 06:31:53.497274
48	21	8	49	2023-03-21 06:31:53.497274
49	32	2	263	2023-03-21 06:31:53.497274
50	103	4	779	2023-03-21 06:31:53.497274
51	26	1	614	2023-03-21 06:31:53.497274
52	127	2	296	2023-03-21 06:31:53.497274
53	57	3	946	2023-03-21 06:31:53.497274
54	102	11	682	2023-03-21 06:31:53.497274
55	39	4	984	2023-03-21 06:31:53.497274
56	57	8	759	2023-03-21 06:31:53.497274
57	121	4	167	2023-03-21 06:31:53.497274
58	32	7	904	2023-03-21 06:31:53.497274
59	53	12	422	2023-03-21 06:31:53.497274
60	6	6	339	2023-03-21 06:31:53.497274
61	29	7	980	2023-03-21 06:31:53.497274
62	71	7	399	2023-03-21 06:31:53.497274
63	88	2	62	2023-03-21 06:31:53.497274
64	114	7	415	2023-03-21 06:31:53.497274
65	141	1	939	2023-03-21 06:31:53.497274
66	7	10	691	2023-03-21 06:31:53.497274
67	83	2	129	2023-03-21 06:31:53.497274
68	116	7	965	2023-03-21 06:31:53.497274
69	40	6	438	2023-03-21 06:31:53.497274
70	21	6	0	2023-03-21 06:31:53.497274
71	19	10	896	2023-03-21 06:31:53.497274
72	78	12	100	2023-03-21 06:31:53.497274
73	107	8	911	2023-03-21 06:31:53.497274
74	7	5	600	2023-03-21 06:31:53.497274
75	142	4	763	2023-03-21 06:31:53.497274
76	6	5	227	2023-03-21 06:31:53.497274
77	59	3	192	2023-03-21 06:31:53.497274
78	135	5	895	2023-03-21 06:31:53.497274
79	53	4	897	2023-03-21 06:31:53.497274
80	77	11	740	2023-03-21 06:31:53.497274
81	34	3	62	2023-03-21 06:31:53.497274
82	121	5	451	2023-03-21 06:31:53.497274
83	87	3	879	2023-03-21 06:31:53.497274
84	67	7	675	2023-03-21 06:31:53.497274
85	133	4	972	2023-03-21 06:31:53.497274
86	120	8	616	2023-03-21 06:31:53.497274
87	114	3	209	2023-03-21 06:31:53.497274
88	113	2	838	2023-03-21 06:31:53.497274
89	49	6	430	2023-03-21 06:31:53.497274
90	40	9	927	2023-03-21 06:31:53.497274
91	141	5	671	2023-03-21 06:31:53.497274
92	7	11	935	2023-03-21 06:31:53.497274
93	53	6	68	2023-03-21 06:31:53.497274
94	120	1	602	2023-03-21 06:31:53.497274
95	137	10	323	2023-03-21 06:31:53.497274
96	98	5	210	2023-03-21 06:31:53.497274
97	119	11	289	2023-03-21 06:31:53.497274
98	69	5	934	2023-03-21 06:31:53.497274
99	12	11	371	2023-03-21 06:31:53.497274
100	7	7	773	2023-03-21 06:31:53.497274
101	138	6	527	2023-03-21 06:31:53.497274
102	125	4	612	2023-03-21 06:31:53.497274
103	132	10	938	2023-03-21 06:31:53.497274
104	118	8	467	2023-03-21 06:31:53.497274
105	39	3	24	2023-03-21 06:31:53.497274
106	51	9	414	2023-03-21 06:31:53.497274
107	4	9	109	2023-03-21 06:31:53.497274
108	67	8	970	2023-03-21 06:31:53.497274
109	93	4	356	2023-03-21 06:31:53.497274
110	147	5	582	2023-03-21 06:31:53.497274
111	20	5	586	2023-03-21 06:31:53.497274
112	114	1	840	2023-03-21 06:31:53.497274
113	112	3	646	2023-03-21 06:31:53.497274
114	31	5	217	2023-03-21 06:31:53.497274
115	63	12	667	2023-03-21 06:31:53.497274
116	135	7	706	2023-03-21 06:31:53.497274
117	86	9	148	2023-03-21 06:31:53.497274
118	79	4	54	2023-03-21 06:31:53.497274
119	25	11	984	2023-03-21 06:31:53.497274
120	87	1	405	2023-03-21 06:31:53.497274
121	70	4	1	2023-03-21 06:31:53.497274
122	130	4	959	2023-03-21 06:31:53.497274
123	97	5	4	2023-03-21 06:31:53.497274
124	94	4	845	2023-03-21 06:31:53.497274
125	124	11	630	2023-03-21 06:31:53.497274
126	84	1	63	2023-03-21 06:31:53.497274
127	145	1	617	2023-03-21 06:31:53.497274
128	36	1	304	2023-03-21 06:31:53.497274
129	119	10	730	2023-03-21 06:31:53.497274
130	33	12	102	2023-03-21 06:31:53.497274
131	80	4	701	2023-03-21 06:31:53.497274
132	22	9	778	2023-03-21 06:31:53.497274
133	49	10	482	2023-03-21 06:31:53.497274
134	61	8	859	2023-03-21 06:31:53.497274
135	62	7	825	2023-03-21 06:31:53.497274
136	93	10	853	2023-03-21 06:31:53.497274
137	7	8	121	2023-03-21 06:31:53.497274
138	73	7	456	2023-03-21 06:31:53.497274
139	121	6	91	2023-03-21 06:31:53.497274
140	89	3	226	2023-03-21 06:31:53.497274
141	104	5	682	2023-03-21 06:31:53.497274
142	91	4	744	2023-03-21 06:31:53.497274
143	88	4	9	2023-03-21 06:31:53.497274
144	125	7	769	2023-03-21 06:31:53.497274
145	21	2	517	2023-03-21 06:31:53.497274
146	115	9	142	2023-03-21 06:31:53.497274
147	149	8	798	2023-03-21 06:31:53.497274
148	115	11	305	2023-03-21 06:31:53.497274
149	127	5	871	2023-03-21 06:31:53.497274
150	137	6	731	2023-03-21 06:31:53.497274
151	31	6	177	2023-03-21 06:31:53.497274
152	56	10	599	2023-03-21 06:31:53.497274
153	65	1	9	2023-03-21 06:31:53.497274
154	88	10	48	2023-03-21 06:31:53.497274
155	29	9	567	2023-03-21 06:31:53.497274
156	50	3	530	2023-03-21 06:31:53.497274
157	149	6	867	2023-03-21 06:31:53.497274
158	15	1	412	2023-03-21 06:31:53.497274
159	115	12	450	2023-03-21 06:31:53.497274
160	61	5	811	2023-03-21 06:31:53.497274
161	96	2	241	2023-03-21 06:31:53.497274
162	120	2	389	2023-03-21 06:31:53.497274
163	16	10	256	2023-03-21 06:31:53.497274
164	39	1	5	2023-03-21 06:31:53.497274
165	151	1	0	2023-03-21 07:18:28.887707
166	151	3	9	2023-03-21 07:18:28.887707
\.


--
-- TOC entry 3370 (class 0 OID 25949)
-- Dependencies: 217
-- Data for Name: organizations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.organizations (id, name) FROM stdin;
1	Runolfsson, West and Hayes
2	Simonis, Gottlieb and Zboncak
3	Buckridge - Stoltenberg
4	Schamberger - Wisozk
5	Hahn - Ratke
6	La Varitos
\.


--
-- TOC entry 3389 (class 0 OID 0)
-- Dependencies: 214
-- Name: agents_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.agents_id_seq', 41, true);


--
-- TOC entry 3390 (class 0 OID 0)
-- Dependencies: 218
-- Name: charges_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.charges_id_seq', 12, true);


--
-- TOC entry 3391 (class 0 OID 0)
-- Dependencies: 222
-- Name: delinquents_charges_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.delinquents_charges_id_seq', 166, true);


--
-- TOC entry 3392 (class 0 OID 0)
-- Dependencies: 220
-- Name: delinquents_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.delinquents_id_seq', 151, true);


--
-- TOC entry 3393 (class 0 OID 0)
-- Dependencies: 216
-- Name: organizations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.organizations_id_seq', 6, true);


--
-- TOC entry 3206 (class 2606 OID 25947)
-- Name: agents agents_cellphone_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.agents
    ADD CONSTRAINT agents_cellphone_key UNIQUE (cellphone);


--
-- TOC entry 3208 (class 2606 OID 25945)
-- Name: agents agents_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.agents
    ADD CONSTRAINT agents_pkey PRIMARY KEY (id);


--
-- TOC entry 3214 (class 2606 OID 25963)
-- Name: charges charges_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.charges
    ADD CONSTRAINT charges_pkey PRIMARY KEY (id);


--
-- TOC entry 3220 (class 2606 OID 26004)
-- Name: countries countries_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.countries
    ADD CONSTRAINT countries_name_key UNIQUE (name);


--
-- TOC entry 3218 (class 2606 OID 25989)
-- Name: delinquents_charges delinquents_charges_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.delinquents_charges
    ADD CONSTRAINT delinquents_charges_pkey PRIMARY KEY (id);


--
-- TOC entry 3216 (class 2606 OID 25971)
-- Name: delinquents delinquents_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.delinquents
    ADD CONSTRAINT delinquents_pkey PRIMARY KEY (id);


--
-- TOC entry 3210 (class 2606 OID 25956)
-- Name: organizations organizations_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.organizations
    ADD CONSTRAINT organizations_name_key UNIQUE (name);


--
-- TOC entry 3212 (class 2606 OID 25954)
-- Name: organizations organizations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.organizations
    ADD CONSTRAINT organizations_pkey PRIMARY KEY (id);


--
-- TOC entry 3221 (class 2606 OID 25972)
-- Name: delinquents delinquents_agent_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.delinquents
    ADD CONSTRAINT delinquents_agent_id_fkey FOREIGN KEY (agent_id) REFERENCES public.agents(id);


--
-- TOC entry 3223 (class 2606 OID 25995)
-- Name: delinquents_charges delinquents_charges_charge_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.delinquents_charges
    ADD CONSTRAINT delinquents_charges_charge_id_fkey FOREIGN KEY (charge_id) REFERENCES public.charges(id);


--
-- TOC entry 3224 (class 2606 OID 25990)
-- Name: delinquents_charges delinquents_charges_delinquent_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.delinquents_charges
    ADD CONSTRAINT delinquents_charges_delinquent_id_fkey FOREIGN KEY (delinquent_id) REFERENCES public.delinquents(id);


--
-- TOC entry 3222 (class 2606 OID 25977)
-- Name: delinquents delinquents_organization_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.delinquents
    ADD CONSTRAINT delinquents_organization_id_fkey FOREIGN KEY (organization_id) REFERENCES public.organizations(id);


-- Completed on 2023-03-21 07:21:14

--
-- PostgreSQL database dump complete
--

